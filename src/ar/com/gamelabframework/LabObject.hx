/**
* LabObjet.hx
* version 0.0.03
*  
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* Martin Javier Di Licia <mjdiliscia@gmail.com>>
*  
* The basic component. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

package ar.com.gamelabframework;

import nme.display.Sprite;
import nme.Lib;
import nme.events.Event;
import ar.com.gamelabframework.utils.Console;

class LabObject extends Sprite {
	
	public var BASE_WIDTH:Float;
	public var BASE_HEIGHT:Float;
	
	public var typeId:String;
	public var frameActive(default,set):Bool;
	private var lastFrameTime:Int;
	
	public var maximizadoX:Bool;
	public var maximizadoY:Bool;

	private var data:String;
	
	public static var lang:String;	
	
	public function new() {
		super();
		// TODO cambiar por el tamano de pantalla decladaro antes del resiceo automatico.
		BASE_WIDTH = 854;
		BASE_HEIGHT = 480;
		//LabObject.lang = nme.system.Capabilities.language;
		if(LabObject.lang==null || LabObject.lang=="")
			LabObject.lang = "en";		
		frameActive = false;
		maximizadoX = false;
		maximizadoY = false;
		typeId = "LabObject";
	}

	private function adjustY():Void {
		//return;
		var usedScale:Float =  Math.min( Lib.current.stage.stageHeight / BASE_HEIGHT, Lib.current.stage.stageWidth / BASE_WIDTH);
		var ysize = usedScale * BASE_HEIGHT;
		if ( maximizadoY ) return;
		if (Lib.current.stage.stageHeight > ysize) {
			y = y +( ( (Lib.current.stage.stageHeight - ysize) / 2)/usedScale);
		}
	}

	private function adjustX():Void {
		//return;
		var usedScale:Float =  Math.min( Lib.current.stage.stageHeight / BASE_HEIGHT, Lib.current.stage.stageWidth / BASE_WIDTH);
		var xsize = usedScale * BASE_WIDTH;
		if ( maximizadoX ) return;
		if (Lib.current.stage.stageWidth > xsize) {
			//var scala:Float = BASE_WIDTH / Lib.current.stage.stageWidth;			
			x = x +( ( (Lib.current.stage.stageWidth - xsize) / 2 )/usedScale);
		}
	}
	
	private function _update(event:Event):Void {
		var deltaTime:Float = (Lib.getTimer()-lastFrameTime)/1000.0;
		update(deltaTime);
		lastFrameTime = Lib.getTimer();
	}
	
	private function update(deltaTime:Float):Void {
		Console.warning("Executing update from LabObject, this should be overritten");
	}
	
	private function set_frameActive(active:Bool):Bool {
		if (active && !frameActive) {
			Console.notice("Activating frameActive");
			addEventListener(Event.ENTER_FRAME, _update);
			lastFrameTime = Lib.getTimer();
			frameActive = true;
		}
		
		if (!active && frameActive) {
			Console.notice("Deactivating frameActive");
			removeEventListener(Event.ENTER_FRAME, _update);
			frameActive = false;
		}
		
		return frameActive;
	}

	public inline function getCenterX():Float
	{
		return( x + this.width / 2 );
	}
	
	public inline function getCenterY():Float
	{
		return( y + this.height / 2 );
	}

	public function getWidth():Float
	{
		return( this.width  );
	}
	
	public function getHeight():Float
	{
		return( this.height  );
	}
	
	public function destroy():Void {
		removeEventListener(Event.ENTER_FRAME, _update);
	}
	
	public function getData():String {
		return data;
	}

	public function setData(newData:String):Void {
		data=newData;
	}	
}