package ar.com.gamelabframework;

class Pair<T,U> {
	public var first:T;
	public var second:U;
	
	public function new(?f:T, ?s:U) {
		if (f != null)
			first = f;
		if (s != null)
			second = s;
	}
}