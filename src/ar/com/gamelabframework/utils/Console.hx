/**
* Console.hx
* version 0.0.03
* 
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* Martin Javier Di Licia <mjdiliscia@gmail.com>>
* 
* A multiplataform log ulitys. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.utils;

#if (cpp)
import sys.io.FileOutput;
#end

class Console {
	public static var showError:Bool=true;
	public static var showWarning:Bool=true;
	public static var showNotice:Bool=true;
	public static var showDebug:Bool=true;
	
	#if (cpp)
	private static var logFile:FileOutput;
	#end
	
	static public function error(err:String, ?toLog:Bool):Void {
		if (!showError) return;
		#if (cpp)
		Sys.print ("ERROR: "+err+"\n");
		#end
		if (toLog) log("ERROR: "+err+"\n");
	}
	
	static public function warning(err:String, ?toLog:Bool):Void {
		if (!showWarning) return;
		#if (cpp)
		Sys.print ("WARNING: "+err+"\n");
		#end
		if (toLog) log("WARNING: "+err+"\n");
	}
	
	static public function notice(err:String, ?toLog:Bool):Void {
		if (!showNotice) return;
		#if (cpp)
		Sys.print ("NOTICE: "+err+"\n");
		#end
		if (toLog) log("NOTICE: "+err+"\n");
	}
	
	static public function debug(logMsg:String, ?toLog:Bool):Void {
		if (!showDebug) return;
		#if (cpp)
		Sys.print ("DEBUG: "+logMsg+"\n");
		#end
		if (toLog) log("DEBUG: "+logMsg+"\n");
	}
	
	static public function show(text:String, ?toLog:Bool):Void {
		#if (cpp)
		Sys.print (text+"\n");
		#end
		if (toLog) log(text+"\n");
	}
	
	static public function log(logText:String):Void {
		#if (cpp)
		if (logFile == null) logFile = sys.io.File.write("console.log");
		logFile.writeString(logText);
		#end
	}

	
	static public function tarce(info:String):Void {
		if (!showDebug) return;
		#if ( debug || android)
			if (info != null) 
				trace(info);
		#end
	}
	
	public function new() {
	}
}