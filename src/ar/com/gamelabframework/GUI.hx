/**
* Gui.hx
* version 0.0.03
*  
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* Martin Javier Di Liscia <mjdiliscia@gmail.com>>
* 
* Factory objet for gui components. This is part of the GameLab compoenent.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework;

import ar.com.gamelabframework.gui.VHBox;
import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.Assets;
import nme.display.Sprite;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.events.MouseEvent;

import ar.com.gamelabframework.gui.Button;
import ar.com.gamelabframework.gui.Slider;
import ar.com.gamelabframework.gui.Image;
import ar.com.gamelabframework.gui.Frame;
import ar.com.gamelabframework.gui.FrameActive;
import ar.com.gamelabframework.gui.Label;
import ar.com.gamelabframework.gui.Input;
import ar.com.gamelabframework.gui.FillingBar;
import ar.com.gamelabframework.gui.Arrow;
import ar.com.gamelabframework.gui.SwitchButton;

class GUI extends LabObject {
	
	public static var assets:Map<String, String>;
	private var objects:Map<String, LabObject>;
	
	public function new() {
		super();
		typeId = "GUI";
		objects = new Map<String, LabObject>();
	}
	
	private function buildGUI(guiSpecsFile):Void {
		var xmlString:String = nme.Assets.getText(guiSpecsFile);
		var xml:Xml = Xml.parse(xmlString);		
		var guiObject:Xml;
		for (guiObject in xml.elements()) {
			var go:LabObject = createWidget(guiObject, this);
			if (go != null) {
				addChild(go);			
				objects.set(guiObject.get("id"), go);
				go.adjustX();
				go.adjustY();
			}
		}
	}
	
	static public function createWidget(specs:Xml, callingGUI:GUI) {
		var widget:LabObject = null;
		if (specs.exists("id")) {
			switch (specs.nodeName) {
				case "Button":
					widget = createButton(specs, callingGUI);					
				case "SwitchButton":
					widget = createSwitch(specs, callingGUI);					
				case "Slider":
					widget = createSlider(specs,callingGUI);
				case "Image":
					widget = createImage(specs,callingGUI);
				case "Frame":
					widget = createFrame(specs,callingGUI);
				case "FrameActive":
					widget = createFrameActive(specs,callingGUI);
				case "Label":
					widget = createLabel(specs,callingGUI);
				case "Input":
					widget = createInput(specs,callingGUI);
				case "FillingBar":
					widget = createFillingBar(specs, callingGUI);
				case "VBox":
					widget = createVBox(specs, callingGUI);
				case "HBox":
					widget = createHBox(specs, callingGUI);
				case "Arrow":
					widget = createArrow(specs, callingGUI);
				default:
					Console.warning(specs.nodeName + " isn't a known guiObject type, skipping it...");
			}
		}
		return widget;
	}
	
	private function getFunction(funcName:String):Dynamic->Void {
		Console.warning("Base getFunction called for "+funcName+", nothing assigned");
		return function(notUsed:Dynamic):Void { };
	}
	
	static public function createButton(button:Xml, callingGUI:GUI):Button {
		var func:Dynamic->Void = callingGUI.getFunction(button.get("clickFnc"));
		var buttonObject:Button = new Button(button, func);
		return buttonObject;
	}

	static public function createSwitch(switchXml:Xml, callingGUI:GUI):Button {
		var func:Dynamic->Void = callingGUI.getFunction(switchXml.get("clickFnc"));
		var switchObject:SwitchButton = new SwitchButton(switchXml, func);
		return switchObject;
	}
	
	static public function createSlider(slider:Xml, callingGUI:GUI):Slider {
		var func:Dynamic->Void = callingGUI.getFunction(slider.get("changeFnc"));
		var sliderObject:Slider = new Slider(slider, func);
		return sliderObject;
	}
	
	static public function createImage(image:Xml, callingGUI:GUI):Image {
		var imageObject:Image = new Image(image);
		return imageObject;
	}

	static public function createFrame(frame:Xml, callingGUI:GUI):Frame {
		var frameObject:Frame = new Frame(frame, callingGUI);
		return frameObject;
	}

	static public function createFrameActive(frame:Xml, callingGUI:GUI):Frame {
		var funcClick:Dynamic->Void = callingGUI.getFunction(frame.get("clickFnc"));
		var funcMove:Dynamic->Void = callingGUI.getFunction(frame.get("moveFnc"));
		var frameObject:FrameActive = new FrameActive(frame, callingGUI, funcClick, funcMove);
		return frameObject;
	}
	
	static public function createLabel(label:Xml, callingGUI:GUI):Label {
		var labelObject:Label = new Label(label);
		return labelObject;
	}

	static public function createInput(input:Xml, callingGUI:GUI):Input {
		var inputObject:Input = new Input(input);
		return inputObject;
	}
	
	static public function createFillingBar(fillingBar:Xml, callingGUI:GUI):LabObject {
		var fillingBarObject:FillingBar = new FillingBar(fillingBar);
		return fillingBarObject;
	}
	
	static public function createVBox(vBox:Xml, callingGUI:GUI):LabObject {
		var vBoxObject:VHBox = new VHBox(vBox, callingGUI, BoxKind.Vertical);
		return vBoxObject;
	}
	
	static public function createHBox(vBox:Xml, callingGUI:GUI):LabObject {
		var vBoxObject:VHBox = new VHBox(vBox, callingGUI, BoxKind.Horizontal);
		return vBoxObject;
	}
	
	static public function createArrow(arrow:Xml, callingGUI:GUI):Arrow {
		var arrowObject:Arrow = new Arrow(arrow);
		return arrowObject;
	}
	
	public function getObject(id:String):LabObject {
		return objects.get(id);
	}
	
	public function addDialog(x, y, frame:Frame, guiSpecsFile:String ):Void {			
		var xmlString:String = nme.Assets.getText(guiSpecsFile);
		var xml:Xml = Xml.parse(xmlString);		
		var guiObject:Xml;
		for (guiObject in xml.elements()) {
			var go:LabObject = createWidget(guiObject, this);
			if (go != null) {
				frame.addGUIOffsetObject(x, y, go, guiObject.get("id"));
			}
		}
	}		
	
}
