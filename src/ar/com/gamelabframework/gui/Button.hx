/**
* Button.hx
* version 0.0.03
*  
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* Martin Javier Di Licia <mjdiliscia@gmail.com>>
*  
* A Button component. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.gui;

import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.display.DisplayObject;
import nme.display.Sprite;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.events.MouseEvent;
import nme.Assets;
import nme.geom.Rectangle;
import nme.text.TextFormat;
import nme.text.TextFormatAlign;

class Button extends LabObject {
	private var bitmap:Bitmap;
	private var pressAsset:Bitmap;
	private var greyBack:Bitmap;
	public var buttonId(default, null):String;
	public var layerAfterText:Int;
	private var greyMask:Sprite;
	private var borderMask:Sprite;
	private var greyBorderMask:Sprite;
	private var drawBorder:Bool;
	private var myClickFunc:Dynamic->Void;
	private var label:Label;
	private var w:Int;
	private var h:Int;	
	private var clickFunction:Dynamic->Void;
	private var clickArea:Sprite;
	private var image:Image;
	private var imageGrey:Image;
	private var mouseIsDown:Bool;

	private var boderSize:Int;
	private var hiColor:Int;
	private var lowColor:Int;
	
	public function new(button:Xml, clickFunc:Dynamic->Void) {
		super();
		
		myClickFunc=clickFunc;
		mouseIsDown = false;
		
		typeId = "Button";
		buttonId = button.get("id");
		
		bitmap = new Bitmap( Assets.getBitmapData("assets/"+GUI.assets.get( button.get("assetId") )) );

		x = Std.parseInt(button.get("x"));
		y = Std.parseInt(button.get("y"));
		
		if (button.exists("w")) {
			w = Std.parseInt(button.get("w"));			
			bitmap.scaleX = w / bitmap.width;
		}
		else {
			w = cast(bitmap.width, Int);
		}
			
		if (button.exists("h")) {
			h = Std.parseInt(button.get("h"));
			bitmap.scaleY = h / bitmap.height;
		}
		else 
		{
			h = cast(bitmap.height, Int);
		}
		
		addChild(bitmap);
		layerAfterText=1;			
		
		if (button.exists("data")) {
			data = button.get("data");
		}

		pressAsset = null;
		if (button.exists("pressAssetId")) {
			pressAsset = new Bitmap( Assets.getBitmapData("assets/" + GUI.assets.get( button.get("pressAssetId") )) );
			pressAsset.scaleX = w / pressAsset.width;													
			pressAsset.scaleY = h / pressAsset.height;							
			addChild(pressAsset);
			pressAsset.visible=false;
			layerAfterText++;			
		}
		
		if (button.exists("greyAssetId")) {
			greyBack = new Bitmap( Assets.getBitmapData("assets/" + GUI.assets.get( button.get("greyAssetId") )) );
			greyBack.scaleX = w / greyBack.width;				
			greyBack.scaleY = h / greyBack.height;
			addChild(greyBack);
			greyBack.visible=false;
			layerAfterText++;			
		}
		else {
			greyBack = new Bitmap( Assets.getBitmapData("assets/"+GUI.assets.get( button.get("assetId") )) );
			greyBack.alpha = 0.5;
			greyBack.scaleX = w / greyBack.width;				
			greyBack.scaleY = h / greyBack.height;			
			//graphics.beginFill(lowColor);		
			//greyBack.graphics.drawRect(0,0,w,h);
			//greyBack.graphics.endFill();						
			addChild(greyBack);
			greyBack.visible=false;
			layerAfterText++;			
		}
				
		image = null;
		if (button.exists("image")) {
			var imageString:String = "<Image id=\"\" x=\"0\" y=\"0\" assetId=\""+button.get("image")+"\"/>";
			var imageXml = Xml.parse(imageString).elements().next();
			
			image = new Image(imageXml);			
			image.x = (w-image.width)/2;
			image.y = (h-image.height)/2;			
			image.mouseEnabled = false;
			
			addChild(image);
			layerAfterText++;			
		}
		imageGrey = null;
		if (button.exists("greyImage")) {
			var imageString:String = "<Image id=\"\" x=\"0\" y=\"0\" assetId=\""+button.get("greyImage")+"\"/>";
			var imageXml = Xml.parse(imageString).elements().next();
			
			imageGrey = new Image(imageXml);			
			imageGrey.x = (w-imageGrey.width)/2;
			imageGrey.y = (h-imageGrey.height)/2;			
			imageGrey.mouseEnabled = false;
			imageGrey.visible = false;
			
			addChild(imageGrey);
			layerAfterText++;			
		}

		var color:Int =0xFFFFFF;	
		if (button.exists("fontColor"))
			color= Std.parseInt(button.get("fontColor"));
		
		var font="VeraSe";
		//var font="bdcartoonshout";
		if (button.exists("font"))
			font=button.get("font");
			
		var sizefont=12;
		if (button.exists("fontSize"))
			sizefont=Std.parseInt(button.get("fontSize"));
				
		var text_lang:String = "";
		if (button.exists("text")) {				
			text_lang = button.get("text");		
		}
		if (button.exists("text" + LabObject.lang) )  {
			text_lang = button.get("text" + LabObject.lang);		
		}
						
		if (text_lang != "") {	
			
			//Cuento la lineas		
			/*
			var index:Int=0;
			var lineas=0;
			do {
				index=text_lang.indexOf("\n",index+1);
				lineas++;
			} while(index>0);								
			var desplazamiento = 0;//(height- (lineas*sizefont*5/4))/2;								
			//var desplazamiento = (h- (lineas*sizefont+(lineas-1)*sizefont))/2;								
			*/
			var labelString:String = "<Label id=\"\" x=\"0\" y=\"" + 0 + "\""; 
			labelString += " w=\""+w+"\" h=\""+h+"\" text=\""+text_lang+"\"";
			labelString += " font=\""+font+"\" fontSize=\""+sizefont+"\" fontColor=\""+color+"\" ";						
			if (button.exists("fontGlowColor")) {
				labelString += "fontGlowColor=\""+Std.parseInt(button.get("fontGlowColor"))+"\" ";
			}
			labelString += "/>";							
			
			var labelXml = Xml.parse(labelString).elements().next();
			label = new Label(labelXml);
			var format:TextFormat = label.getTextFormat();
			//var desplazamiento = (h- ( lineas*sizefont + lineas-1*format.leading))/2;									
			format.align = TextFormatAlign.CENTER;
			label.setTextFormat(format);
			//trace(format.leading);
			
			//label.y += (h - label.getTextHeight() * 5 / 4) / 2;
			//label.y += h/2 - label.getTextHeight()/2;
			label.y += (h - label.getTextHeight())/2;
			
			//label.height -= label.y;
			//trace( lineas + " lineas de " + sizefont + " =  " + label.getTextHeight() );				
			//label.height -= desplazamiento;
			
			addChild(label);
			
			label.setText(label.getText());
		}
		else
		{
			var labelString:String = "<Label id=\"\" x=\"0\" y=\"1\" w=\"0\" h=\"0\" text=\"\" font=\""+font+"\" fontSize=\""+sizefont;
			labelString += "\" fontColor=\""+color+"\" ";						
			if (button.exists("fontGlowColor")) {
				labelString += "fontGlowColor=\""+Std.parseInt(button.get("fontGlowColor"))+"\" ";
			}
			labelString += "/>";			
			var labelXml = Xml.parse(labelString).elements().next();
			label = new Label(labelXml);			
		}
		
		// Bordes
		drawBorder = false;
		boderSize = 0;
		if (button.exists("border")) {
			drawBorder = true;			
			borderMask = new Sprite();			
			hiColor = 0x505050;
			if (button.exists("borderHiColor"))
				hiColor = Std.parseInt(button.get("borderHiColor"));
			else {
				if (button.exists("assetId"))
					hiColor = getDefaultHiColor(button.get("assetId"));
			}
			lowColor = 0xc0c0c0;
			if (button.exists("borderHiColor"))
				lowColor= Std.parseInt(button.get("borderLowColor"));			
			else {
				if (button.exists("assetId"))
					lowColor = getDefaultLowColor(button.get("assetId"));
			}
			boderSize = Std.parseInt(button.get("border"));
			addBorder(borderMask, boderSize, hiColor, lowColor);						
			addChild(borderMask);
			borderMask.mouseEnabled = false;
			
			greyBorderMask = new Sprite();			
			var greyhiColor:Int = 0x505050;
			if (button.exists("grayBorderHiColor"))
				greyhiColor = Std.parseInt(button.get("grayBorderHiColor"));
			else {
				if (button.exists("greyAssetId"))
					greyhiColor = getDefaultHiColor(button.get("greyAssetId"));
			}
			var greylowColor:Int = 0xc0c0c0;
			if (button.exists("grayBorderHiColor"))
				greylowColor= Std.parseInt(button.get("grayBorderLowColor"));			
			else {
				if (button.exists("greyAssetId"))
					greylowColor = getDefaultLowColor(button.get("greyAssetId"));
			}
			addBorder(greyBorderMask, boderSize, greyhiColor, greylowColor);
			addChild(greyBorderMask);
			greyBorderMask.mouseEnabled = false;
			greyBorderMask.visible=false;			
		}
		
		var enmascarar:Bool = true;
		if ( button.exists("mask") ) {
			enmascarar = (button.get("mask").toUpperCase() == "TRUE" );		
		}
		
		greyMask = new Sprite();
		greyMask.graphics.beginFill(0x000000, .3);
		greyMask.graphics.drawRect(0,0,w,h);
		greyMask.graphics.endFill();
		greyMask.visible = false;		
		greyMask.mouseEnabled = false;
		
		if ( enmascarar ) {		
			mask = greyMask;
		}
		
		addChild(greyMask);
		
		addListeners();
		
		clickFunction = clickFunc;

	}
	
	public function addListeners() {
		if( drawBorder || pressAsset!=null ) {
			addEventListener(MouseEvent.MOUSE_DOWN, myMouseDown);
			addEventListener(MouseEvent.MOUSE_UP, myMouseUp);
			addEventListener(MouseEvent.MOUSE_OUT, myMouseOut);
			//addEventListener(MouseEvent.MOUSE_OVER, myMouseOver);
		}
		else 
			addEventListener(MouseEvent.CLICK, click);
	}
	
	public function removeListeners() {	
		if ( drawBorder || pressAsset!=null ) {
			removeEventListener(MouseEvent.MOUSE_DOWN, myMouseDown);
			removeEventListener(MouseEvent.MOUSE_UP, myMouseUp);
			removeEventListener(MouseEvent.MOUSE_OUT, myMouseOut);
			//removeEventListener(MouseEvent.MOUSE_OVER, myMouseOver);
		}
		else
			removeEventListener(MouseEvent.CLICK, click);		
	}
	
	public function getImage():Image {
		return image;
	}
	
	public function click(event:MouseEvent):Void {
		//trace("MouseClick");
		if (this.isEnableButton()) {
			if (clickFunction != null) {
				clickFunction(event);
			}
		}	
	}

	public function myMouseDown(event:MouseEvent):Void {	
		//trace("MouseDown");
		if (this.isEnableButton()) {
			mouseIsDown = true;
			
			if ( pressAsset != null ) {
				pressAsset.visible = true;
			}

			if ( drawBorder ) { 
				if (borderMask != null) {
					removeChild(borderMask);
					//borderMask.destroy();
				}					
				borderMask = new Sprite();			
				addBorder(borderMask, boderSize, lowColor, hiColor);
				addChild(borderMask);
				borderMask.mouseEnabled = false;
			}
		}
	}

	public function myMouseUp(event:MouseEvent):Void {
		//trace("MouseUp");
		if (this.isEnableButton()) {
			
			if ( pressAsset != null ) {
				pressAsset.visible = false;
			}
			
			if ( drawBorder ) { 
				if (borderMask != null) {
					removeChild(borderMask);
					//borderMask.destroy();
				}
				borderMask = new Sprite();			
				addBorder(borderMask, boderSize, hiColor, lowColor);
				addChild(borderMask);
				borderMask.mouseEnabled = false;
			}
			
		}
		
		if (mouseIsDown && clickFunction != null) {
			mouseIsDown = false;
			clickFunction(event);
		}
		mouseIsDown = false;		
	}

	public function myMouseOut(event:MouseEvent):Void {
		//trace("MouseOut");
		mouseIsDown = false;
		if (this.isEnableButton()) {
			if ( pressAsset != null ) {
				pressAsset.visible = false;
			}
			
			if ( drawBorder ) { 
				if (borderMask != null) {
					removeChild(borderMask);
					//borderMask.destroy();
				}
				borderMask = new Sprite();			
				addBorder(borderMask, boderSize, hiColor, lowColor);
				addChild(borderMask);
				borderMask.mouseEnabled = false;
			}
		}		
	}
	
	public function disableButton(){	
		if(!greyBack.visible) {
			//Console.tarce("Desactivando Boton"); 
			greyBack.visible = true;
			if( label.getText()!="")
				greyMask.visible = true;		
				
			if (imageGrey != null) {
				image.visible = false;
				imageGrey.visible = true;
			}

			if (drawBorder) {
				greyBorderMask.visible = true;
				borderMask.visible = false;							
			}
			mouseEnabled = false;
			removeListeners();
		}
	}

	public function isEnableButton():Bool {
		return !greyBack.visible;
	}
	
	public function enableButton(){
		if(	greyBack.visible) {
			//Console.tarce("Activando Boton"); 
			greyBack.visible = false;
			if( label.getText()!="")
				greyMask.visible = false;					
			if (imageGrey != null) {
				image.visible = true;
				imageGrey.visible = false;
			}
			if (drawBorder) {
				greyBorderMask.visible = false;			
				borderMask.visible = true;			
			}
			mouseEnabled = true;
			addListeners();
		}
	}
		
	public function AddImg(x:Float, y:Float, image:String):Image 	{
		var imageString:String = "<Image id=\"\" x=\"0\" y=\"0\" assetId=\""+image+"\"/>";
		var imageXml = Xml.parse(imageString).elements().next();
		var image:Image = new Image(imageXml);			
		image.x = x;
		image.y = y;
		image.mouseEnabled = false;			
		addChildAt(image, layerAfterText);
		return image;
	}

	public function AddScaledImg(x:Float, y:Float, w:Float, h:Float, image:String):Image 	{
		var imageString:String = "<Image id=\"\" x=\""+x+"\" y=\""+y+"\" w=\""+w+"\" h=\""+h+"\" assetId=\""+image+"\"/>";
		var imageXml = Xml.parse(imageString).elements().next();
		var image:Image = new Image(imageXml);			
		image.mouseEnabled = false;			
		addChildAt(image,layerAfterText);
		return image;
	}	
	
	public function getText():String {
		return label.getText();
	}

	public function setText(text:String):Void {
		label.setText(text);	
		label.y = (h-label.getTextHeight())/2;
	}

	public function addBorder(borderMask:Sprite, size:Int = 3, hiColor:Int, lowColor:Int) {
		//Console.tarce( "button border=" + size + " in" + " x:"+ x + " y:"+y + " w:"+w + " h:"+h );
		//var cullingMask:Sprite = new Sprite();
		var i:Int = 0;
		
		borderMask.graphics.beginFill(lowColor);		
		borderMask.graphics.drawRect(w-size,0,size,h-size);
		//Console.tarce( "border low right " + " x:" + (w - size) + " y:" + 0 + " w:" + size + " h:" + h );		
		borderMask.graphics.drawRect(0,h-size,w,size);
		//Console.tarce( "border low down" + " x:" + 0 + " y:" + (h - size) + " w:" + w + " h:" + size );
		borderMask.graphics.endFill();
		
		
		borderMask.graphics.beginFill(hiColor);		
		for ( i in 0...(size))
		{

			borderMask.graphics.drawRect(i, 0, 1 , h-i );
			//Console.tarce( "border hi left " + i + "> x:" + i + " y:" + 0 + " w:" + 1 + " h:" + (h - i) );
			
			borderMask.graphics.drawRect(size, i, w-i-size-1, 1);			
			//Console.tarce( "border hi up " + i + "> x:"+ 0 + " y:"+i + " w:"+ (w-i) + " h:"+ 1 );

			
		}
		borderMask.graphics.endFill();
		
		
		borderMask.mouseEnabled = false;
		//addChildAt(cullingMask,layerAfterText);
	}		
	
	public function getDefaultHiColor( asset:String ):Int {
		//Console.tarce( asset );
		switch( asset ) {
			case "Red":
				return 0xff7c7c;
			case "Blue":
				return 0x6e6eff;
			case "Green":
				return 0x80fe80;
			case "Back":
				return 0x80fe80;
			case "Grey":
				return 0x505050;				
			case "GreyBack":
				return 0x505050;				
				//return 0xb0b0b0;
			case "Yellow":
				return 0xfff0b0;
			case "Black":
				return 0x303030;
			case "Black50":
				return 0x303030;
			case "Black75":
				return 0x303030;
			case "White":	
				return 0xf0f0f0;		
			default:
				return 0x505050;		
		}
		return 0;
	}
	
	public function getDefaultLowColor( asset:String ):Int {
		//Console.tarce( asset );
		switch( asset ) {
			case "Red":
				return 0xad0101;
			case "Blue":
				return 0x030399;
			case "Green":
				//trace ("green");
				return 0x009020;					
			case "Back":
				//trace ("green");
				return 0x009020;					
			case "GreyBack":
				return 0xb0b0b0;
			case "Grey":
				return 0xb0b0b0;
				//return 0x505050;
			case "Yellow":
				return 0x7c7c02;
			case "Black":
				return 0x000000;
			case "Black50":
				return 0x000000;
			case "Black75":
				return 0x000000;
			case "White":	
				return 0x505050;		
			default:
				//Console.tarce("default");
				return 0xc0c0c0;
		}
		return 0;
	}
	
	override public function destroy():Void {			
		super.destroy();
		removeListeners();
		myClickFunc = null;	
	}
	
	public function refreshHitArea(parentBounds:Rectangle, comparator:DisplayObject):Void {
		var bounds:Rectangle = getBounds(comparator);
		var intersection:Rectangle = bounds.intersection(parentBounds);
		
		if ((intersection.width < w*0.9 || intersection.height < h*0.9)) {
			mouseEnabled = false;
			mouseChildren = false;
//			alpha = 0.5;
		} else {
			mouseEnabled = true;
			mouseChildren = true;
//			alpha = 1;
		}
	}
}