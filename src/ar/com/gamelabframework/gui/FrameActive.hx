/**
* FrameActive.hx
* version 0.0.03
* 
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* 
* A layout container for other components. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.gui;

import ar.com.gamelabframework.GUI;
import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.DisplayObject;
import nme.display.Sprite;
import nme.Assets;
import nme.geom.Rectangle;
import nme.events.MouseEvent;
import nme.Lib;

class FrameActive extends Frame {

	private var mouseIsDown:Bool;
	private var clickFunction:Dynamic->Void;
	private var moveFunction:Dynamic->Void;
	private var mouseCaptured:Bool;
	private	var mousePosX:Float;
	private var mousePosY:Float;
	
	inline public function getMousePosX():Float {
		return mousePosX;
	}
	
	inline public function getMousePosY():Float {
		return mousePosY;
	}
	
	public function new(frame:Xml, cGUI:GUI, clickFunc:Dynamic->Void, moveFunc:Dynamic->Void) {
		super(frame, cGUI);
		typeId = "FrameActive";
		mouseEnabled = true;
		mouseCaptured = false;
		clickFunction = clickFunc;
		moveFunction = moveFunc;
		mousePosX = 0;
		mousePosY = 0;
		addListeners();
	}
	
	override public function destroy():Void {			
		super.destroy();
		removeListeners();
	}

	public function ensable() {
		if ( mouseEnabled ) return;
		mouseEnabled = true;
		addListeners();
	}
	
	public function disable() {
		mouseEnabled = false;
		mouseCaptured = false;
		removeListeners();
	}
		
	private function addListeners() {
		addEventListener(MouseEvent.MOUSE_DOWN, myMouseDown);
		addEventListener(MouseEvent.MOUSE_UP, mouseEventsRelease);
		addEventListener(MouseEvent.MOUSE_OUT, mouseEventsRelease);
		addEventListener(MouseEvent.MOUSE_MOVE, myMouseMove);
	}
	
	public function removeListeners() {	
		removeEventListener(MouseEvent.MOUSE_DOWN, myMouseDown);
		removeEventListener(MouseEvent.MOUSE_UP, mouseEventsRelease);
		removeEventListener(MouseEvent.MOUSE_OUT, mouseEventsRelease);
		removeEventListener(MouseEvent.MOUSE_MOVE, myMouseMove);
	}	

	public function isMouseCaptured():Bool {
		return mouseCaptured;		
	}
	
	private function myMouseDown(event:MouseEvent):Void {			
		mouseCaptured = true;				
		mousePosX = (event.localX * scaleX);
		mousePosY = (event.localY * scaleX);		
		if (clickFunction != null) {
			clickFunction(event);
		}			
	}

	private function myMouseMove(event:MouseEvent):Void {			
		if (event.buttonDown) {
			// Scroll						
			// Si no scroleamos
			if (moveFunction != null) {
				moveFunction(event);
			}			
			//mousePosX = (event.localX * scaleX);
			//mousePosY = (event.localY * scaleX);
		}			
	}
	
	private function mouseEventsRelease(event:MouseEvent):Void {
		mouseCaptured = false;
	}	
}