/**
* FillingBar.hx
* version 0.0.03
*  
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* Martin Javier Di Licia <mjdiliscia@gmail.com>>
* 
* A filling bar component. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.gui;

import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.Sprite;
import nme.Assets;
import nme.text.TextFormat;
import nme.text.TextFormatAlign;

class FillingBar extends LabObject {
	private var bitmap:Bitmap;
	private var fill:Sprite;
	private var cullingMask:Sprite;
	private var align:String;
	private var label:Label;
	public var perc(default,null):Float;
	
	public function new(fillingBar:Xml) {
		super();
		typeId = "FillingBar";
		
		bitmap = new Bitmap( Assets.getBitmapData("assets/"+GUI.assets.get( fillingBar.get("assetId") )) );
		addChild(bitmap);
		x = Std.parseInt(fillingBar.get("x"));
		y = Std.parseInt(fillingBar.get("y"));
		
		var w:Float = bitmap.width;
		var h:Float = bitmap.height;
		if (fillingBar.exists("w"))
			w = Std.parseInt(fillingBar.get("w"));
		if (fillingBar.exists("h"))		
			h = Std.parseInt(fillingBar.get("h"));
		bitmap.scaleY = h / bitmap.height;
		bitmap.scaleX = w / bitmap.width;
		
		fill = new Sprite();
		var relleno:Bitmap = new Bitmap( Assets.getBitmapData("assets/" + GUI.assets.get( fillingBar.get("fillAssetId") )) );
		fill.addChild(relleno);
		var difx:Float = 0;
		var dify:Float = 0;
		if (fillingBar.exists("fillDispX"))		
			difx = Std.parseFloat(fillingBar.get("fillDispX"));
		if (fillingBar.exists("fillDispY"))		
			dify = Std.parseFloat(fillingBar.get("fillDispY"));		
		fill.x = difx;
		fill.y = dify;		
		fill.width =  (w - (difx * 2 * bitmap.scaleX));
		fill.height = (h - (dify * 2 * bitmap.scaleY ));
		fill.scaleY = (h-(dify*2)) / relleno.height;
		fill.scaleX = (w-(difx*2)) / relleno.width;

		align = "RIGHT";	
		if (fillingBar.exists("align"))
			align = fillingBar.get("align");			
		
		var initialFill:Float = Std.parseFloat(fillingBar.get("initialFill"));
		setFill(initialFill);
		
		addChild(fill);
				
		var color:Int =0xFFFFFF;	
		if (fillingBar.exists("fontColor"))
			color= Std.parseInt(fillingBar.get("fontColor"));		var font="VeraSe";
		//var font="bdcartoonshout";
		if (fillingBar.exists("font"))
			font = fillingBar.get("font");			
					
		var sizefont=12;
		if (fillingBar.exists("fontSize"))
			sizefont=Std.parseInt(fillingBar.get("fontSize"));
		var text:String="";
		if (fillingBar.exists("text")) 
			text=fillingBar.get("text");			
						
		var labelString:String = "<Label id=\"\" x=\"+0+\" y=\"+0+\" w=\""+Std.parseInt(fillingBar.get("w"))+"\" h=\""+Std.parseInt(fillingBar.get("h"))+"\" ";
		labelString += " text=\""+text+"\" font=\""+font+"\" align=\"center\" fontSize=\""+sizefont;
		labelString += "\" color=\""+color+"\" ";						
		if (fillingBar.exists("fontGlowColor")) 
			labelString += "fontGlowColor=\""+Std.parseInt(fillingBar.get("fontGlowColor"))+"\" ";		
		labelString += "/>";
			
		var labelXml = Xml.parse(labelString).elements().next();
		label = new Label(labelXml);
		//var format:TextFormat = label.getTextFormat();
		//format.align = TextFormatAlign.CENTER;
		//label.setTextFormat(format);
		//label.y += (height - label.getTextHeight() * 5 / 4) / 2;
		label.y += (height - sizefont -1) / 2;
		addChild(label);
			
		label.setText(label.getText());				

	}
	
	public function setFill(fillVal:Float):Void {
		if (fillVal > 1)
			fillVal = 1;
		if (fillVal < 0)
			fillVal = 0;
		
		perc = fillVal;
		
		if (cullingMask != null)
			fill.removeChild(cullingMask);
		cullingMask = new Sprite();
		cullingMask.graphics.clear();
		cullingMask.graphics.beginFill(0xffffff, 1);
		if ( align.toUpperCase() == "LEFT" || align.toLowerCase() == "L" ) 
			//cullingMask.graphics.drawRect(fill.width - (fillVal * fill.width), 0, fill.width, fill.height);
			cullingMask.graphics.drawRect(( (1-fillVal) * fill.width), 0,fill.width, fill.height);
		else
			cullingMask.graphics.drawRect(0,0,fillVal*fill.width,fill.height);
		cullingMask.graphics.endFill();
		fill.mask = cullingMask;
		fill.addChild(cullingMask);
	}
	
	public function setText(text:String):Void {
		label.setText(text);
	}
	
	public function getText():String {
		return label.getText();
	}	
}