/**
* VHBox.hx
* version 0.0.03
* 
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* Martin Javier Di Licia <mjdiliscia@gmail.com>>
* 
* A layout container for other components, that allow arrange the component 
* whit some spare in horizontal o vertical form. 
* This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.gui;

import ar.com.gamelabframework.GUI;
import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.DisplayObject;
import nme.display.Sprite;
import nme.Assets;
import nme.geom.Rectangle;

enum BoxKind {
	Horizontal;
	Vertical;
}

class VHBox extends Frame {
	
	private var boxKind:BoxKind;
	private var sizesList:List<Float>;
	private var betweenSpacing:Float;
	private var autoResize:Bool;
	
	public function new(frame:Xml, cGUI:GUI, bk:BoxKind) {
		super(frame, cGUI);
		
		boxKind = bk;
		
		autoResize = frame.exists("sizes");
		if (autoResize) {
			if (frame.exists("spacing")) {
				betweenSpacing = Std.parseFloat(frame.get("spacing"));
			} else {
				betweenSpacing = 0;
			}
			
			sizesList = new List<Float>();
			var sizesStr:Array<String> = frame.get("sizes").split(",");
			var size:String;
			var asterisk:Bool = false;
			for (size in sizesStr) {
				size = StringTools.ltrim(StringTools.trim(size));
				if (size == "*") {
					if (asterisk) {
						Console.error("More than one asterisk on: " + frame.get("id"));
						return;
					}
					
					sizesList.add( -1);
				} else if (size.substr(0, 1) == "%" && Std.parseFloat(size.substr(1)) > 0) {
					sizesList.add( (boxKind==BoxKind.Vertical?visualHeight:visualWidth) * Std.parseFloat(size.substr(1)) / 100 );
				} else if (Std.parseFloat(size) > 0) {
					sizesList.add(Std.parseFloat(size));
				} else {
					Console.warning("VHBox size in list is " + size + ", using 0 instead");
					sizesList.add(0);
				}
			}
		}
		
		refreshChildren();
	}
	
	private function refreshChildren() {
		if (autoResize) {
			refreshChildrenPosSizing();
		} else {
			refreshChildrenPositioning();
		}
	}
	
	private function refreshChildrenPosSizing() {
		Console.debug("Resizing");
		var widgetList:List<LabObject> = new List<LabObject>();
		
		for (i in 0...container.numChildren) {
			var child:LabObject = cast(container.getChildAt(i), LabObject);
			if (StringTools.startsWith(Type.getClassName(Type.getClass(child)), "ar.com.gamelabframework.gui.")) {
				widgetList.add(child);
			}
		}
		
		var totalSize:Float = 0;
		var currentSize:Float;
		for (currentSize in sizesList) {
			Console.debug(Std.string(currentSize));
			if (currentSize != -1)
				totalSize += currentSize;
		}
		var remainingSize:Float = (boxKind==BoxKind.Vertical?visualHeight:visualWidth) - totalSize - betweenSpacing * (sizesList.length-1);
		
		var nextPos:Float = 0;
		var widget:LabObject;
		var iter:Iterator<Float> = sizesList.iterator();
		for (widget in widgetList) {
			var newSize:Float;
			if (!iter.hasNext()) {
				break;
			} else {
				newSize = iter.next();
			}
			if (newSize == -1) newSize = remainingSize;
			
			widget.width = boxKind == BoxKind.Horizontal?newSize:visualWidth;
			widget.height = boxKind == BoxKind.Vertical?newSize:visualHeight;
			widget.x = boxKind == BoxKind.Horizontal?nextPos:0;
			widget.y = boxKind == BoxKind.Vertical?nextPos:0;
			
			Console.debug("Resized: (" + widget.x + "," + widget.y + ")(" + widget.width + "," + widget.height + ")");
			
			nextPos += (boxKind == BoxKind.Vertical?widget.height:widget.width) + betweenSpacing;
		}
	}
	
	private function refreshChildrenPositioning() {
		var widgetList:List<LabObject> = new List<LabObject>();
		var widgetsSize:Float = 0;
		
		for (i in 0...container.numChildren) {
			var child:LabObject = cast(container.getChildAt(i), LabObject);
			if (StringTools.startsWith(Type.getClassName(Type.getClass(child)), "ar.com.gamelabframework.gui.")) {
				widgetList.add(child);
				widgetsSize += boxKind==BoxKind.Vertical?child.height:child.width;
			}
		}
		
		var remanentSize = (boxKind == BoxKind.Vertical?height:width) - widgetsSize;
		var increment = remanentSize / (widgetList.length - 1);
		
		var nextPos:Float = 0;
		var widget:LabObject;
		for (widget in widgetList) {
			widget.x = boxKind == BoxKind.Horizontal?nextPos:((visualWidth - widget.width) / 2);
			widget.y = boxKind == BoxKind.Vertical?nextPos:((visualHeight - widget.height) / 2);
			nextPos += (boxKind == BoxKind.Vertical?widget.height:widget.width) + increment;
		}
	}
	
	override public function addGUIObjectAt(object:LabObject, id:String, index:Int) {
		super.addGUIObjectAt(object, id, index);		
		refreshChildren();
	}

	override public function addGUIObject(object:LabObject, id:String) {
		super.addGUIObject(object, id);		
		refreshChildren();
	}	
	
	override public function removeGUIObject(id:String) {
		super.removeGUIObject(id);
		refreshChildren();
	}
}