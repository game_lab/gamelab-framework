/**
* Image.hx
* version 0.0.03
* 
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* Martin Javier Di Licia <mjdiliscia@gmail.com>>
* 
* A simple bitmap component. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.gui;

import ar.com.gamelabframework.LabObject;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.Sprite;
import nme.Assets;
import nme.Lib;

import nme.events.MouseEvent;

class Image extends LabObject {
	private var bitmap:Bitmap;
	
	public function new(image:Xml) {
		super();
		typeId = "Image";
		
		bitmap = new Bitmap( Assets.getBitmapData("assets/" + GUI.assets.get( image.get("assetId") )) );
		//trace("\n" + image.get("assetId")+":");
		x = Std.parseInt(image.get("x"));
		y = Std.parseInt(image.get("y"));
		var ho:Float=bitmap.height;
		var wo:Float=bitmap.width;
		var h:Float = bitmap.height;		
		var w:Float = bitmap.width;
		
		
		//trace ( "bitmap.width:" + bitmap.width );
		//trace ( "bitmap.height:" + bitmap.height );
		
		//trace("stageWidth:" + Lib.current.stage.stageWidth);
		//trace("stageHeight:" + Lib.current.stage.stageHeight);
		//trace("stage.scaleX:"+Lib.current.stage.scaleX);
		//trace("stage.scaleY:"+Lib.current.stage.scaleY);
		//trace("stage.scaleMode:" + Lib.current.stage.scaleMode);
		
		//trace( "current.scaleX:" + Lib.current.scaleX);
		//trace( "current.scaleY:" + Lib.current.scaleY);

		//trace( "current.width:" + Lib.current.width);
		//trace( "current.height:" + Lib.current.height);
		

		if (parent != null) {
			//trace("parent.width:" + parent.width);
			//trace("parent.height:" + parent.height);	
		}
		if (image.exists("w")) {
			if (image.get("w") == "*") {
				maximizadoX = true;
				//trace ( "Lib.current.scaleX:" + Lib.current.scaleX ) ;
				//trace ( "Lib.current.scaleY:" + Lib.current.scaleY ) ;
				//trace ( "Lib.current.height:" + Lib.current.height ) ;
				//trace ( "Lib.current.width:" + Lib.current.width ) ;
				//trace ( "Lib.current.stage.stageHeight:" + Lib.current.stage.stageHeight ) ;
				//trace ( "Lib.current.stage.height:" + Lib.current.stage.height ) ;
				//trace ( "Lib.current.stage.stageWidth:" + Lib.current.stage.stageWidth ) ;
				//trace ( "Lib.current.stage.width:" + Lib.current.stage.width ) ;
				
				//trace ( "Lib.current.stage.fullScreenWidth:" + Lib.current.stage.fullScreenWidth ) ;
				//trace ( "Lib.current.stage.fullScreenWidth:" + Lib.current.stage.fullScreenWidth ) ;

				if (parent == null) {
					w = Lib.current.stage.stageWidth;
				}
				else
					w = parent.width;
			}
			else {
				w = Std.parseFloat(image.get("w"));			
			}
		}
		if (image.exists("h")) {
			if (image.get("h") == "*") {
				maximizadoY = true;
				if (parent == null)
					h = Lib.current.stage.stageHeight;
				else
					h = parent.height;
			}
			else
				h = Std.parseFloat(image.get("h"));		
		}
	
		//trace ( "w:" + w + " h:" + h + " wo:" + wo + " ho:" + ho);
		if(w!=bitmap.width || h!=bitmap.height)
		{				
			
			var scaleX:Float =  Lib.current.stage.stageWidth / BASE_WIDTH;
			//trace ( "scaleX:" + scaleX);			
			var scaleY:Float =  Lib.current.stage.stageHeight/BASE_HEIGHT;
			//trace ( "scaleY:" + scaleY);
			
			var c:Float=1;
			if ( scaleY < scaleX) 
			c = scaleY;
			else
			c = scaleX;
			
			//bitmap.scaleY = (h / ho)  ;
			bitmap.scaleY = (h / ho) / c  ;
			//bitmap.scaleY = (h / ho) / scaleY  ;


			//var difx:Float =  bitmap.width/Lib.current.stage.stageWidth  ;
			//trace ( "difx:" + difx);

			//bitmap.scaleX = w / wo ;
			bitmap.scaleX = (w / wo) / c;
			//bitmap.scaleX = (w / wo) / scaleX;
		}	
		
		if (image.exists("alpha")) {
			bitmap.alpha = Std.parseFloat(image.get("alpha"));
		}
		
		mouseEnabled = false;

		
		//trace ( "bitmap.width:" + bitmap.width );
		//trace ( "bitmap.height:" + bitmap.height );
		//trace ( "bitmap.scaleX:" + bitmap.scaleX );
		//trace ( "bitmap.scaleY:" + bitmap.scaleY );
		
		addChild(bitmap);
		
	}
	
	public function getBitmap():Bitmap {
		return bitmap;
	}
	
	public function setBitmap(assetId:String) {
		var bitmapScaleX:Float = bitmap.scaleX;
		var bitmapScaleY:Float = bitmap.scaleY;
		removeChild(bitmap);
		
		bitmap = new Bitmap( Assets.getBitmapData("assets/"+GUI.assets.get( assetId )) );
		bitmap.scaleX = bitmapScaleX;
		bitmap.scaleY = bitmapScaleY;
		addChild(bitmap);
	}
	
	public function hFlip(facing:Bool)
	{
		if( facing ) { 
			if(scaleX>0)
				x -= width;
			scaleX = -1 * Math.abs(scaleX) ;
		}
		else {
			if(scaleX<0)
				x += width;
			scaleX = Math.abs(scaleX);
		}
		cacheAsBitmap = false;
	}	

	public function vFlip(facing:Bool)
	{
		if( facing ) { 
			if(scaleX>0)
				y -= height;
			scaleY = -1 * Math.abs(scaleY) ;
		}
		else {
			if(scaleY<0)
				y += height;
			scaleY = Math.abs(scaleY);
		}
		cacheAsBitmap = false;
	}	
	
}