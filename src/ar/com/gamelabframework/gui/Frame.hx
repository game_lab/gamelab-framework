/**
* Frame.hx
* version 0.0.03
* 
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* Martin Javier Di Licia <mjdiliscia@gmail.com>>
* 
* A layout container for other components. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.gui;

import ar.com.gamelabframework.GUI;
import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.DisplayObject;
import nme.display.Sprite;
import nme.Assets;
import nme.geom.Rectangle;
import nme.Lib;

class Frame extends LabObject {
	private var bitmap:Bitmap;
	private var container:Sprite;
	private var objects:Map<String, LabObject>;
	private var cols:Int;
	
	private var visualWidth:Float;
	private var visualHeight:Float;
	private var parentGUI:GUI;
	
	public function new(frame:Xml, cGUI:GUI) {
		super();
		typeId = "Frame";
		var w:Float=0;
		var h:Float=0;
		objects = new Map<String, LabObject>();
		parentGUI = cGUI;
		
		x = Std.parseInt(frame.get("x"));
		y = Std.parseInt(frame.get("y"));
		cols= Std.parseInt(frame.get("cols"));
		
		
		/*
		if (frame.exists("w"))
			w = Std.parseInt(frame.get("w"));
		if (frame.exists("h"))
			h = Std.parseInt(frame.get("h"));
		
		if ( frame.exists("assetId") )
			bitmap = new Bitmap( Assets.getBitmapData("assets/"+GUI.assets.get( frame.get("assetId") )) );
		else
		{
			bitmap = new Bitmap();
			width = w;
			height = h;
		}	
		addChild(bitmap);
		*/
		
		
		
		// ------------------------------------	
		/*
		if (frame.exists("w"))
			bitmap.scaleX = w / bitmap.width;
		if (frame.exists("h"))
			bitmap.scaleY = h / bitmap.height;
		*/							
		// ------------------------------------	 resizeado

			
		if (frame.exists("w")) {
			if (frame.get("w") == "*") {
				if (parent == null) {
					w = Lib.current.stage.stageWidth;
					//trace( "current w: " + w);
				}
				else {
					w = parent.width;
					//trace( "parent w: " + w);
				}
			}
			else {
				w = Std.parseFloat(frame.get("w"));			
			}
		}
		if (frame.exists("h")) {
			if (frame.get("h") == "*")
				if (parent == null) {
					h = Lib.current.stage.stageHeight;
					//trace( "current h: " + h);
				}
				else {
					h = parent.height;
					//trace( "parent h: " + h);
				}
			else  {
				h = Std.parseFloat(frame.get("h"));		
			}
		}

		if ( frame.exists("assetId") )
			bitmap = new Bitmap( Assets.getBitmapData("assets/" + GUI.assets.get( frame.get("assetId") )) );
		else
		{
			bitmap = new Bitmap();
			//bitmap.width = w;
			//bitmap.height = h;
		}	
		
		addChild(bitmap);
			
		var ho:Float=bitmap.height;
		var wo:Float = bitmap.width;
		
		if(w!=bitmap.width || h!=bitmap.height)
		{				
			var scaleX:Float =  Lib.current.stage.stageWidth / BASE_WIDTH;
			var scaleY:Float =  Lib.current.stage.stageHeight/ BASE_HEIGHT;
			var c:Float=1;
			if ( scaleY < scaleX) 
			c = scaleY;
			else
			c = scaleX;
			//bitmap.scaleY = (h / ho) / c  ;
			//bitmap.scaleX = (w / wo) / c;
			
			bitmap.scaleX = w / bitmap.width;
			bitmap.scaleY = h / bitmap.height;
		}				
			
		// ------------------------------------	
											
		visualWidth = width;
		visualHeight = height;

		var enmascarar:Bool = true;
		if ( frame.exists("mask") ) {
			enmascarar = (frame.get("mask").toUpperCase() == "TRUE" );		
		}
		
		if ( enmascarar ) {
			var cullingMask:Sprite = new Sprite();
			cullingMask.graphics.beginFill(0xffffff, 1);
			cullingMask.graphics.drawRect(0,0,visualWidth,visualHeight);
			cullingMask.graphics.endFill();
			cullingMask.visible = false;
			cullingMask.mouseEnabled = false;		
			mask = cullingMask;				
			addChild(cullingMask);
		}
		
		container = new Sprite();
		if (frame.exists("backColor"))
		{		
			var backColor:Int= Std.parseInt(frame.get("backColor"));
			Console.tarce( "frame de color: "+backColor);
			container.graphics.beginFill(backColor, 1);
			container.graphics.drawRect(0,0,visualWidth,visualHeight);
			container.graphics.endFill();
		}
		container.mouseEnabled = false;
					
		addChild(container);		
		
		addChildren(frame);

		mouseEnabled = false;
		
	}
	
	private function addChildren(frame:Xml) {
		var guiObject:Xml;
		for (guiObject in frame.elements()) {
			var go:LabObject = GUI.createWidget(guiObject, parentGUI);
			if (go != null) {
				addGUIObject(go, guiObject.get("id"));
			}
		}
	}

	public function getCols():Float {
		return cols;
	}
	
	public override function getHeight():Float {
		return visualHeight;
	}
	public override function getWidth():Float {
		return visualWidth;
	}
	
	public function getBitmap():Bitmap {
		return bitmap;
	}

	public function Hide() {
		visible=false;
	}	
	
	public function hideBackground() {	
		bitmap.visible = false;		
	}	
	
	public function Show() {
		visible=true;
	}

	
	public function setBitmap(assetId:String) {
		bitmap = new Bitmap( Assets.getBitmapData("assets/"+GUI.assets.get( assetId )) );
	}

	public function addGUIObjectAt(object:LabObject, id:String, index:Int) {
		container.addChildAt(object, index);
		objects.set(id, object);
		if (Type.getClassName(Type.getClass(object)) == "ar.com.gamelabframework.gui.Button") {
			var bounds:Rectangle = new Rectangle(0,0,visualWidth,visualHeight);
			cast(object, Button).refreshHitArea(bounds, this);
		}
	}
	
	public function addGUIObject(object:LabObject, id:String) {
		container.addChild(object);
		objects.set(id, object);
		if (Type.getClassName(Type.getClass(object)) == "ar.com.gamelabframework.gui.Button") {
			var bounds:Rectangle = new Rectangle(0,0,visualWidth,visualHeight);
			cast(object, Button).refreshHitArea(bounds, this);
		}
	}

	public function addGUIOffsetObject(x:Float, y:Float, object:LabObject, id:String) {		
		object.x += x;
		object.y += y;
		addGUIObject(object, id);
	}
	
	
	public function getGUIObject(id:String) {
		return objects.get(id);
	}
	
	public function removeGUIObject(id:String) {
		if (!objects.exists(id)) {
			Console.warning("Attempting to remove inexistent "+id+", skipping");
			return;
		}
		
		container.removeChild(objects.get(id));
		objects.remove(id);
	}
	
	public function getOverWidth():Float {
		return Math.max(0, container.width - visualWidth);
	}
	public function getOverHeight():Float {
		return Math.max(0, container.height - visualHeight);
	}
	
	public function getContainerWidth():Float {
		return Math.max(visualWidth, container.width);
	}
	public function getContainerHeight():Float {
		return Math.max(visualHeight, container.height);
	}
	
	public function setDisplacementX(disp:Float):Void {
		container.x = -disp;
		refreshChildrenHitArea();
	}
	public function getDisplacementX():Float {
		return -container.x;
	}
	public function setDisplacementY(disp:Float):Void {
		container.y = -disp;
		refreshChildrenHitArea();
	}
	public function getDisplacementY():Float {
		return -container.y;
	}
	
	public function isVisibleInY(ypos:Float):Bool {
		return ( (ypos+container.y)>0 && (ypos+container.y)<visualHeight );
	}
	public function isVisibleInX(xpos:Float):Bool {
		return ( (xpos+container.x)>0 && (xpos+container.x)<visualWidth  );
	}
	
	public function isObjectVisibleInX(o:LabObject):Bool {
		return (isVisibleInX(o.x) || isVisibleInX(o.x + o.width) );
	}

	public function isObjectVisibleInY(o:LabObject):Bool {
		return (isVisibleInY(o.y) || isVisibleInY(o.y + o.width) );
	}

	public function isObjectVisible(o:LabObject):Bool {
		return ( 
					(isVisibleInX(o.x) || isVisibleInX(o.x + o.width) ) && 
					(isVisibleInY(o.y) || isVisibleInY(o.y + o.width) ) 
				);
	}

	public function refreshChildrenHitArea():Void {
		var bounds:Rectangle = new Rectangle(0,0,visualWidth,visualHeight);
		for (i in 0...container.numChildren) {
			var child = container.getChildAt(i);
			if (Type.getClassName(Type.getClass(child)) == "ar.com.gamelabframework.gui.Button") {
				cast(child, Button).refreshHitArea(bounds, this);
			}
		}
	}
}