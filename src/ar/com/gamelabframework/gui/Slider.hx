/**
* Slider.hx
* version 0.0.03
* 
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* Martin Javier Di Licia <mjdiliscia@gmail.com>>
* 
* A basic slider component. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.gui;

import ar.com.gamelabframework.LabObject;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.events.MouseEvent;
import nme.Assets;

class Slider extends LabObject {
	private var handle:Bitmap;
	private var enableHandle:Bitmap;
	private var disableHandle:Bitmap;
//	public var value(getHandleValue,setHandleValue):Float;
	
	private var handleValue:Float;
	private var sliderWidth:Int;
	private var maxValue:Float;
	private var minValue:Float;
	
	private var mouseCaptured:Bool;
	
	private var changeFunction:Dynamic->Void;
	
	public function new(slider:Xml, changeFunc:Dynamic->Void) {
		super();
		typeId = "Slider";
		
		var bitmap:Bitmap = new Bitmap( Assets.getBitmapData("assets/"+GUI.assets.get( slider.get("backAssetId") )) );
		addChild(bitmap);
		enableHandle = new Bitmap( Assets.getBitmapData("assets/"+GUI.assets.get( slider.get("handleAssetId") )) );
		handle = enableHandle;
		addChild(handle);
		if (slider.exists("greyHandleAssetId"))
		{
			disableHandle = new Bitmap( Assets.getBitmapData("assets/"+GUI.assets.get( slider.get("greyHandleAssetId") )) );
			addChild(disableHandle);
			disableHandle.visible=false;
		}
		
		x = Std.parseInt(slider.get("x"));
		y = Std.parseInt(slider.get("y"));
		
		minValue = Std.parseFloat(slider.get("min"));
		maxValue = Std.parseFloat(slider.get("max"));
		
		if (slider.exists("w"))
			scaleX = Std.parseInt(slider.get("w")) / bitmap.width;
		if (slider.exists("h"))
			scaleY = Std.parseInt(slider.get("h")) / bitmap.height;
		
		sliderWidth = Math.round(bitmap.width * scaleX);
		
		changeFunction = changeFunc;
		
		if (changeFunc != null) {
			addEventListener(MouseEvent.MOUSE_DOWN, mouseEvents);
			addEventListener(MouseEvent.MOUSE_MOVE, mouseEvents);
			addEventListener(MouseEvent.MOUSE_UP, mouseEventsRelease);
			addEventListener(MouseEvent.MOUSE_OUT, mouseEventsRelease);
		}
	}
	
	public function getHandleValue():Float {
		return handleValue;
	}
	
	public function getCurrentXPos():Float {
		return handle.x;
	}
	
	public function getCurrentYPos():Float {
		return handle.y; 
	}

	
	public function setHandleValue(v:Float):Float {
		handleValue = v;
		if (handleValue > maxValue) handleValue = maxValue;
		if (handleValue < minValue) handleValue = minValue;
		
		handle.x = (handleValue-minValue)/(maxValue-minValue) * (sliderWidth/scaleX-handle.width);
		
		return handleValue;
	}
	
	public function getMin():Float {
		return minValue;
	}
	public function getMax():Float {
		return maxValue;
	}
	
	public function setMin(newMin:Float):Float {
		minValue = newMin;
		setHandleValue(getHandleValue());	// For assure value to stay inside range
		return getHandleValue();
	}
	public function setMax(newMax:Float):Float {
		maxValue = newMax;
		setHandleValue(getHandleValue());	// For assure value to stay inside range
		return getHandleValue();
	}
	
	public function isMouseCaptured():Bool {
		return mouseCaptured;		
	}
	
	private function mouseEvents(event:MouseEvent):Void {
		if (event.buttonDown) {
			mouseCaptured = true;
			var prevValue = getHandleValue();
			setHandleValue((event.localX*scaleX - handle.width*scaleX/2)/(sliderWidth - handle.width*scaleX) * (maxValue-minValue) + minValue);
			
			if (prevValue != getHandleValue())
				changeFunction(event);
		}
	}
	
	private function mouseEventsRelease(event:MouseEvent):Void {
		mouseCaptured = false;
	}
		
	public function getPerc():Float {
		return (getHandleValue()-minValue)/(maxValue-minValue);
	}
	
	public function disable(){	
		if(enableHandle.visible) {
			//Console.tarce("Desactivando Boton"); 
			enableHandle.visible = false;
			disableHandle.visible = true;
			handle = disableHandle;
			removeEventListener(MouseEvent.MOUSE_DOWN, mouseEvents);
			removeEventListener(MouseEvent.MOUSE_MOVE, mouseEvents);
			removeEventListener(MouseEvent.MOUSE_UP, mouseEventsRelease);
			removeEventListener(MouseEvent.MOUSE_OUT, mouseEventsRelease);
		}
	}

	public function enable(){
		if(	!enableHandle.visible) {
			//Console.tarce("Activando Boton"); 
			enableHandle.visible = true;
			disableHandle.visible = false;
			handle = enableHandle;
			addEventListener(MouseEvent.MOUSE_DOWN, mouseEvents);
			addEventListener(MouseEvent.MOUSE_MOVE, mouseEvents);
			addEventListener(MouseEvent.MOUSE_UP, mouseEventsRelease);
			addEventListener(MouseEvent.MOUSE_OUT, mouseEventsRelease);
		}
	}
	
	
}