/**
* Input.hx
* version 0.0.03
* 
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* Martin Javier Di Licia <mjdiliscia@gmail.com>>
* 
* A text imput component. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.gui;

import ar.com.gamelabframework.LabObject;
import nme.Assets;
import nme.geom.Rectangle;

import nme.display.Sprite;

import nme.events.Event;
import nme.events.TextEvent;
import nme.events.MouseEvent;
import nme.events.KeyboardEvent;

import nme.text.TextField;
import nme.text.TextFormat;
import nme.text.TextFieldType;
import nme.text.TextFormatAlign;
import nme.filters.GlowFilter;


import nme.events.FocusEvent;
import nme.display.Bitmap;
import nme.display.BitmapData;


class Input extends LabObject {
	private var _textField:TextField;
	private var _defaultText:String;
	private var _borderColor:Int;
	private var _selectColor:Int;
	private var _borderRect:Sprite;
	private var _selectRect:Sprite;
	private var _borderOn:Bool;
	
	var _text:String;
	
	public function new(input:Xml) {
		super();
		typeId = "Input";
		
		_text = "";		
		var w:Float = Std.parseFloat(input.get("w"));
		var h:Float = Std.parseFloat(input.get("h"));			
		
		if (!input.exists("h"))			
			h = Std.parseInt(input.get("fontSize"))+5;		
			
		x = Std.parseInt(input.get("x"));
		y = Std.parseInt(input.get("y"));
						
		mouseEnabled = true;
		mouseChildren = true;
		
		//var font = Assets.getFont ("assets/fonts/BD_Cartoon_Shout.ttf");
		//var font = Assets.getFont ("assets/fonts/badaboom.ttf");
		
		var font = Assets.getFont ("assets/fonts/bdcartoonshout.ttf");		
		if (input.exists("font"))
			font = Assets.getFont ("assets/fonts/"+input.get("font")+".ttf");

		var color:Int =0xFFFFFF;	
		if (input.exists("fontColor"))
			color= Std.parseInt(input.get("fontColor"));
						
		var format:TextFormat = new TextFormat(font.fontName, Std.parseInt(input.get("fontSize")), color );
		
		if (input.exists("align"))
		{
			switch (input.get("align")) {
			case "center":
				format.align = TextFormatAlign.CENTER;			
			case "justify":
				format.align = TextFormatAlign.JUSTIFY;			
			case "left":
				format.align = TextFormatAlign.LEFT;			
			case "right":
				format.align = TextFormatAlign.RIGHT;			
			default:
			}			
		}
		else
		{
			format.align = TextFormatAlign.CENTER;			
		}
				
		_textField = new TextField();
		_textField.defaultTextFormat = format;		
		
		_textField.type = TextFieldType.INPUT;		

		if (input.exists("password")) {
			var passwordOn:Bool = (input.get("password")=="true");		
			_textField.displayAsPassword = passwordOn;
		}
		
		_textField.addEventListener(Event.CHANGE, onChange);
		_textField.addEventListener(FocusEvent.FOCUS_IN, onFocusHandler);
		_textField.addEventListener(FocusEvent.FOCUS_OUT, onFocusHandler);
		
		//new TextFormat(font.fontName, Std.parseInt(input.get("fontSize")), color );
		
		_textField.selectable = true;
		_textField.embedFonts = true;
		_textField.multiline = false;
		
		//_textField.defaultTextFormat.align = TextFormatAlign.CENTER;					
			
		var glowColor:Int =0xffffff-color;	
		//Console.tarce("glowclor="+glowColor); 
		if (input.exists("fontGlowColor")) {
			glowColor= Std.parseInt(input.get("fontGlowColor"));
			_textField.filters = [ new  GlowFilter(glowColor, 1, 5, 3,8) ];
		}
				
		_defaultText = input.get("text");		
		_textField.text = _defaultText;		
		
		//Centrado vertical		
		var index:Int=0;
		var lineas=0;
		do
		{
			index=_textField.text.indexOf("\n",index+1);
			lineas++;
		} while(index>0);
		//Console.tarce(lineas+" lineas de "+Std.parseInt(input.get("fontSize"))+" px en una caha de "+h+" de alto");
		var margen:Float=(h-(lineas*_textField.textHeight))/2;
		//Console.tarce("margen="+margen);

		_borderColor = 0x505050; 
		if (input.exists("borderColor")) {
			_borderColor = Std.parseInt(input.get("borderColor"));
		}
		_selectColor = 0xff0000; 
		if (input.exists("selectColor")) {
			_selectColor = Std.parseInt(input.get("selectColor"));
		}
		
		var borderSize:Float = 2;
		_borderOn=false;				
		if (input.exists("backColor"))
		{
			_borderOn=true;				
			var backColor:Int= Std.parseInt(input.get("backColor"));
			
			if (input.exists("borderSize"))
				borderSize = Std.parseFloat(input.get("borderSize"));
				
			var lineas = 1;
			var margen:Float=(h-(lineas*Std.parseInt(input.get("fontSize"))))/2;

			_borderRect = new Sprite();			
			_borderRect.graphics.beginFill(_borderColor, 1);			
			_borderRect.graphics.drawRect(0,0,w,h);
			_borderRect.graphics.endFill();
			_borderRect.mouseEnabled = false;			
			addChild(_borderRect);
			
			_selectRect = new Sprite();			
			_selectRect.graphics.beginFill(_selectColor, 1);			
			//_selectRect.graphics.drawRect(-5-borderSize,-1*margen-borderSize,w+5+borderSize,h+borderSize);
			_selectRect.graphics.drawRect(0,0,w,h);
			//Console.tarce( 0 + ", " + 0 + ", " + w  + ", " + h );
			_selectRect.graphics.endFill();
			_selectRect.mouseEnabled = false;
			_selectRect.visible = false;
			addChild(_selectRect);
			
			var backRect:Sprite = new Sprite();
			backRect.graphics.beginFill(backColor, 1);			
			//backRect.graphics.drawRect(-5,-1*margen,w+5-borderSize,h-borderSize);
			backRect.graphics.drawRect(borderSize, borderSize, w - (borderSize*2), h - (borderSize*2));
			//Console.tarce( borderSize + ", " + borderSize + ", " + (w - (borderSize*2)) + ", " + (h - (borderSize*2)) );
			backRect.graphics.endFill();
			backRect.mouseEnabled = false;
			addChild(backRect);	
		}		
		
		//y+=margen;
		
		_textField.y = margen;
		_textField.x = borderSize*2;
		_textField.background = false;
		_textField.border = false;
		/*
		_textField.background = false;
		_textField.backgroundColor = 0xffffff;
		_textField.border = true;
		_textField.borderColor = _borderColor;
		*/
		
		_textField.width = w-borderSize*4;
		_textField.height = h-margen;		
		
		addChild (_textField);
		
		mouseEnabled = true;
		mouseChildren = true;
		_textField.mouseEnabled = true;
	}
		
	public function setInputPass(passmode:Bool):Void {
		_textField.displayAsPassword = passmode;
	}
	
	public function getInputPass(passmode:Bool):Bool {
		return _textField.displayAsPassword;
	}


	///////////////////////////////////
	// event handlers
	///////////////////////////////////
	
	override private function update(deltaTime:Float):Void
	{
		//Console.tarce("on change: " + _text);
		_textField.text = _text;
		super.update(deltaTime);		
	}
	
	private function onFocusHandler(e : FocusEvent) : Void {
		//Console.tarce( "InputBoxBase.onFocusHandler > e : " + e );
		var contentTxt : String;
		if (e.type == FocusEvent.FOCUS_IN) {
			//_textField.borderColor = _selectColor;
			if(_borderOn)
				_selectRect.visible = true;
			contentTxt = _textField.text;
			//if (contentTxt != _defaultText) {
				_textField.text = "";
				
			//}
		} else if (e.type == FocusEvent.FOCUS_OUT) {
			//_textField.borderColor = _borderColor;
			if(_borderOn)
				_selectRect.visible = false;			
			contentTxt = _textField.text;
			if (contentTxt == "") {
				_textField.text = _defaultText;
			}
		}
	}

	/**
	 * Internal change handler.
	 * @param event The Event passed by the system.
	 */
	function onChange(event:Event):Void
	{
		//Console.tarce("on change");
		_text = _textField.text;
		event.stopImmediatePropagation();
		dispatchEvent(event);
	}
	
	public function getText():String {
		//return _text;
		return _textField.text;
	}

	public function setText(text:String):Void {
		// Don't allow null set text
		if (text == null) return;
		_textField.text = text;
		return;		
	}
	
	public function getDefaultText():String {
		//return _text;
		return _defaultText;
	}
		
	public function setDefaultText(text:String):Void {
		// Don't allow null set text
		if (text == null) return;
		_defaultText = text;
		return;		
	}
	
	public function getTextFormat():TextFormat {
		return _textField.defaultTextFormat;
	}
	
	public function setTextFormat(textFmt:TextFormat):Void {
		_textField.defaultTextFormat = textFmt;
	}
	
	public function getTextHeight():Float {
		return _textField.textHeight;
	}
	
	public function setTab(index:Int) {
		this.tabChildren = true;
		#if(flash)
		_textField.tabEnabled = true;		
		_textField.tabIndex = index;
		#end
	}
	
	public function getTextField():TextField {
		return _textField;
	}
}