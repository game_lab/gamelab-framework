/**
* SwitchButton.hx
* version 0.0.03
*  
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
*  
* A Switch Button component. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.gui;

import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.display.DisplayObject;
import nme.display.Sprite;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.events.MouseEvent;
import nme.Assets;
import nme.geom.Rectangle;
import nme.text.TextFormat;
import nme.text.TextFormatAlign;

class SwitchButton extends Button {
	private var statusActive:Bool;
	
	public function new(button:Xml, clickFunc:Dynamic->Void) {
		super(button, clickFunc);
		statusActive = false;
	}
		
	public override function click(event:MouseEvent):Void {
		//trace("MouseClick");
		if (this.isEnableButton()) {
			if (clickFunction != null) {
				switchState();
				clickFunction(event);
			}
		}	
	}

	public override function myMouseDown(event:MouseEvent):Void {	
		//trace("MouseDown");
		if (this.isEnableButton()) {
			mouseIsDown = true;
			switchState();			
			clickFunction(event);
		}
	}

	public override function myMouseUp(event:MouseEvent):Void {
		//trace("MouseUp");
		if (this.isEnableButton()) {					
			if (mouseIsDown && clickFunction != null) {
				mouseIsDown = false;
			}
			mouseIsDown = false;		
		}
	}

	public override function myMouseOut(event:MouseEvent):Void {
		//trace("MouseOut");
		if (this.isEnableButton()) {
			mouseIsDown = false;
		}		
	}
	
	public function setOn() {		
		if (this.isEnableButton()) {			
			statusActive = true;
			if ( pressAsset != null ) {
				pressAsset.visible = true;
			}

			if ( drawBorder ) { 
				if (borderMask != null) {
					removeChild(borderMask);
					//borderMask.destroy();
				}					
				borderMask = new Sprite();			
				addBorder(borderMask, boderSize, lowColor, hiColor);
				addChild(borderMask);
				borderMask.mouseEnabled = false;
			}
		}
	}
	
	public function setOff() {
		if (this.isEnableButton()) {			
			statusActive = false;		
			if ( pressAsset != null ) {
				pressAsset.visible = false;
			}
			
			if ( drawBorder ) { 
				if (borderMask != null) {
					removeChild(borderMask);
					//borderMask.destroy();
				}
				borderMask = new Sprite();			
				addBorder(borderMask, boderSize, hiColor, lowColor);
				addChild(borderMask);
				borderMask.mouseEnabled = false;
			}
		}
	}

	public function switchState() {
		if (statusActive)
			setOff();
		else 
			setOn();
	}

	public function isOn() {
		return statusActive;
	}

	public function isOff() {
		return !statusActive;
	}
	
}