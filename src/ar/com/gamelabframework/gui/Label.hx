/**
* label.hx
* version 0.0.03
* 
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* Martin Javier Di Licia <mjdiliscia@gmail.com>>
* 
* A text box component. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.gui;

import ar.com.gamelabframework.LabObject;
import nme.Assets;

import nme.display.Sprite;
/*
import nme.events.Events;
import nme.events.TextEvent;
import nme.events.MouseEvent;
import nme.events.KeyboardEvent;
*/
import nme.text.TextField;
import nme.text.TextFormat;
import nme.text.TextFieldType;
import nme.text.TextFormatAlign;
import nme.filters.GlowFilter;

class Label extends LabObject {
	private var textField:TextField;
	
	public function new(label:Xml) {
		super();
		typeId = "Label";
		
		var w:Float = Std.parseFloat(label.get("w"));
		var h:Float = Std.parseFloat(label.get("h"));			
		
		if (!label.exists("h"))			
			h = Std.parseInt(label.get("fontSize"));		
			
		x = Std.parseInt(label.get("x"));
		y = Std.parseInt(label.get("y"));
		
		if (label.exists("backColor"))
		{
			var backColor:Int=Std.parseInt(label.get("fontColor"));
			var backRect:Sprite = new Sprite();
			backRect.graphics.beginFill(backColor, 1);
			backRect.graphics.drawRect(0,0,w,h);
			backRect.graphics.endFill();
			backRect.mouseEnabled = false;
			addChild(backRect);
		}	
		
		//var font = Assets.getFont ("assets/fonts/BD_Cartoon_Shout.ttf");
		//var font = Assets.getFont ("assets/fonts/badaboom.ttf");
		
		var font = Assets.getFont ("assets/fonts/bdcartoonshout.ttf");		
		if (label.exists("font"))
			font = Assets.getFont ("assets/fonts/"+label.get("font")+".ttf");

		var color:Int =0xFFFFFF;	
		if (label.exists("fontColor"))
			color= Std.parseInt(label.get("fontColor"));
						
		var format:TextFormat = new TextFormat(font.fontName, Std.parseInt(label.get("fontSize")), color );
		if (label.exists("align"))
		{
			switch (label.get("align")) {
			case "center":
				format.align = TextFormatAlign.CENTER;			
			case "justify":
				format.align = TextFormatAlign.JUSTIFY;			
			case "left":
				format.align = TextFormatAlign.LEFT;			
			case "right":
				format.align = TextFormatAlign.RIGHT;			
			default:
			}			
		}
		else
		{
			format.align = TextFormatAlign.CENTER;			
		}
		
		textField = new TextField();
		textField.defaultTextFormat = format;		
		//new TextFormat(font.fontName, Std.parseInt(label.get("fontSize")), color );
		
		textField.selectable = false;
		textField.embedFonts = true;
		textField.multiline = true;
		
		//textField.defaultTextFormat.align = TextFormatAlign.CENTER;			
		if (label.exists("data")) {
			data = label.get("data");
		}
					
		var glowColor:Int =0xffffff-color;	
		//Console.tarce("glowclor="+glowColor); 
		if (label.exists("fontGlowColor")) {
			glowColor= Std.parseInt(label.get("fontGlowColor"));
			textField.filters = [ new  GlowFilter(glowColor, 1, 5, 3,8) ];
		}
				
		if ( label.exists("text") ) {
			textField.text = label.get("text");		
		}
		if ( label.exists("text" + LabObject.lang) ) {
			textField.text = label.get("text"+LabObject.lang);		
		}
		
		//Centrado vertical
		/*
		var index:Int=0;
		var lineas=0;
		do
		{
			index=textField.text.indexOf("\n",index+1);
			lineas++;
		} while(index>0);
		//Console.tarce(lineas+" lineas de "+Std.parseInt(label.get("fontSize"))+" px en una caha de "+h+" de alto");
		var margen:Float=(h-(lineas*Std.parseInt(label.get("fontSize"))))/2;
		//Console.tarce("margen="+margen);
		textField.y += margen;
		*/
		
		/*
		textField.background = false;
		textField.backgroundColor = 0xffffff;
		textField.border = true;
		textField.borderColor = 0x505050;
		*/
		textField.width = w;
		textField.height = h;			
		textField.mouseEnabled = false;
		
		addChild (textField);
		
		mouseEnabled = false;
		mouseChildren = false;
	}

	
	public function getText():String {
		return textField.text;
	}
	
	public function setText(text:String):Void {
		textField.text = text;
	}
	
	public function getTextFormat():TextFormat {
		return textField.defaultTextFormat;
	}
	
	public function setTextFormat(textFmt:TextFormat):Void {
		textField.defaultTextFormat = textFmt;
	}
	
	public function getTextHeight():Float {
		return textField.textHeight;
	}
}