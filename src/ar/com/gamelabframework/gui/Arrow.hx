/**
* Arrow.hx
* version 0.0.03
*  
* Authors:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
*  
* A Arrow component. This is part of GameLab framework.
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package ar.com.gamelabframework.gui;

import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.display.DisplayObject;
import nme.display.Sprite;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.events.MouseEvent;
import nme.Assets;
import nme.geom.Rectangle;
import nme.text.TextFormat;
import nme.text.TextFormatAlign;

class Arrow extends LabObject {
	private var bitmap:Bitmap;
	public var arrowId(default, null):String;
	public var xEnd:Float;
	public var yEnd:Float;	
	public var xStart:Float;
	public var yStart:Float;	
	public var w:Float;
	public var h:Float;	
	private var color:Int;
	private var colorHi:Int;
	private var colorLow:Int;
	private var size:Int;
	private var arrowSprite:Sprite;	
	private var type:String;

	public function new(arrow:Xml) {
		super();
		
		typeId = "Arrow";
		arrowId = arrow.get("id");
						
		xStart = Std.parseFloat(arrow.get("x"));
		yStart = Std.parseFloat(arrow.get("y"));
		xEnd = Std.parseFloat(arrow.get("xEnd"));
		yEnd = Std.parseFloat(arrow.get("yEnd"));

		type = "elipse";
		if (arrow.exists("type"))
			type= arrow.get("type");		
		
		size = 10;
		if (arrow.exists("size"))
			size= Std.parseInt(arrow.get("size"));

		w = 0;
		if (arrow.exists("w"))
			w= Std.parseInt(arrow.get("w"));
		
		h = 0;
		if (arrow.exists("h"))
			h= Std.parseInt(arrow.get("h"));
			
		if (arrow.exists("alpha"))
			alpha= Std.parseInt(arrow.get("alpha"));
			
		color = 0xFFFFFF;	
		if (arrow.exists("color"))
			color= Std.parseInt(arrow.get("color"));
							
		arrowSprite = new Sprite();		
		addChild(arrowSprite);		
		setArrowTo( xEnd, yEnd, w, h );
		
		arrowSprite.mouseEnabled = false;
	}
		
	public function setArrowTo(xEndNew:Float, yEndNew:Float, wNew:Float = -1, hNew:Float = -1) {	
		if (wNew < 0) wNew = w;
		if (hNew < 0) hNew = h;
		arrowSprite.graphics.clear();
								
		arrowSprite.graphics.lineStyle(size,color,alpha);	
		arrowSprite.graphics.moveTo(xStart, yStart);
		
		switch ( type ) {
		case "line":
			arrowSprite.graphics.lineTo( xEndNew, yEndNew );									
		case "arrow":
			
			if ( wNew!=0 || hNew!=0 ) {
				var d = Math.max(hNew,wNew);			
				var m = ((yEndNew - yStart) / (xEndNew - xStart));
				//var b = yStart - m * xStart;						
				var sign = 1;
				if ( xEndNew < xStart) sign = -1;
				
				var xp = sign * ((d/2)+(size/2)) * Math.cos(Math.atan(m));
				var yp = sign * ((d / 2) + (size / 2)) * Math.sin(Math.atan(m));							
				
				arrowSprite.graphics.lineTo( xEndNew-xp, yEndNew-yp);					
										
				xp = sign * ((d / 2) + (size / 2)) * Math.cos(Math.atan( -m));			
				yp = sign * ((d / 2) + (size / 2)) * Math.sin(Math.atan( -m));							
															
				/*
				 arrowSprite.graphics.lineStyle(size/2,0xff0000, 0.5);	
				 arrowSprite.graphics.moveTo( xEndNew-xp, yEndNew+yp);
				 arrowSprite.graphics.lineTo( xEndNew + xp, yEndNew - yp );								
				 arrowSprite.graphics.lineStyle(size/2,0x00ff00, 0.5);	
				 arrowSprite.graphics.moveTo( xEndNew-yp, yEndNew-xp);
				 arrowSprite.graphics.lineTo( xEndNew+yp, yEndNew+xp );					
				*/
				var x0 = xEndNew-xp*2;
				var y0 = yEndNew+yp*2;			
				var x1 = xEndNew+yp-xp*2;
				var y1 = yEndNew + xp + yp * 2;
				/*
				 arrowSprite.graphics.lineStyle(size/2,0x0000ff, 0.5);	
				 arrowSprite.graphics.moveTo( x0, y0);
				 arrowSprite.graphics.lineTo( x1, y1);					
				 */
				var x2 = xEndNew-yp-xp*2;
				var y2 = yEndNew - xp + yp * 2;
				/*
				 arrowSprite.graphics.moveTo( x0, y0);
				 arrowSprite.graphics.lineTo( x2, y2);					
				 */

				arrowSprite.graphics.beginFill(color, alpha);
				arrowSprite.graphics.lineStyle(size/2,color,alpha);	
				arrowSprite.graphics.moveTo( x1, y1);			
				arrowSprite.graphics.lineTo( xEndNew, yEndNew );								
				arrowSprite.graphics.lineTo( x2, y2);					
				arrowSprite.graphics.lineTo( x1, y1);
				arrowSprite.graphics.endFill();
			}
			else { 				
				arrowSprite.graphics.lineTo( xEndNew, yEndNew );									
			}		
				
		case "circle":
			var d = Math.max(hNew,wNew);			
			if ( d!=0 || d!=0 ) {
				var m = (yEndNew - yStart) / (xEndNew - xStart);
				var sign = 1;
				if ( xEndNew < xStart) sign = -1;
				var xp = sign * ((d / 2) + (size / 2)) * Math.cos(Math.atan(m));				
				var yp = sign * ((d / 2) + (size / 2)) * Math.sin(Math.atan(m));								
				
				arrowSprite.graphics.lineTo( xEndNew - xp, yEndNew-yp);													
				arrowSprite.graphics.drawEllipse(xEndNew-(d/2), yEndNew-(d/2), (d-(size/2)), (d-(size/2)));					
			}
			else { 				
				arrowSprite.graphics.lineTo( xEndNew, yEndNew );			
				//arrowSprite.graphics.drawEllipse(xEndNew-(wNew/2)-size, yEndNew-(hNew/2)-size, (wNew+size*2), (hNew+size*2));				
				
				arrowSprite.graphics.beginFill(color, alpha);				
				arrowSprite.graphics.drawEllipse(xEndNew-(size), yEndNew-(size), (size*2), (size*2) );					
				arrowSprite.graphics.endFill();		
			}		

		case "solidCircle":
			var d = Math.max(hNew,wNew);			
			if ( d!=0 || d!=0 ) {
				var m = (yEndNew - yStart) / (xEndNew - xStart);
				var sign = 1;
				if ( xEndNew < xStart) sign = -1;
				var xp = sign * ((d / 2) + (size / 2)) * Math.cos(Math.atan(m));				
				var yp = sign * ((d / 2) + (size / 2)) * Math.sin(Math.atan(m));								
				
				arrowSprite.graphics.lineTo( xEndNew - xp, yEndNew-yp);													
				arrowSprite.graphics.beginFill(color, alpha);				
				arrowSprite.graphics.drawEllipse(xEndNew-(d/2), yEndNew-(d/2), (d-(size/2)), (d-(size/2)));					
				arrowSprite.graphics.endFill();		
			}
			else { 				
				arrowSprite.graphics.lineTo( xEndNew, yEndNew );			
				//arrowSprite.graphics.drawEllipse(xEndNew-(wNew/2)-size, yEndNew-(hNew/2)-size, (wNew+size*2), (hNew+size*2));				
				
				arrowSprite.graphics.beginFill(color, alpha);				
				arrowSprite.graphics.drawEllipse(xEndNew-(size), yEndNew-(size), (size*2), (size*2) );					
				arrowSprite.graphics.endFill();		
			}		
						
		default:
			if ( wNew!=0 && hNew!=0 ) {
				
				if (Math.abs((yEndNew - yStart)) >  Math.abs(xEndNew - xStart) ) 
				{
					if ((yEndNew - yStart) > 0) {
						arrowSprite.graphics.lineTo( xEndNew, yEndNew - (hNew / 2) -(size/2) );
					}
					else {
						arrowSprite.graphics.lineTo( xEndNew, yEndNew + (hNew / 2)  );
					}
				}
				else {
					if ((xEndNew - xStart) > 0) {
						arrowSprite.graphics.lineTo( xEndNew - (wNew/2) -(size/2) , yEndNew  );
					}
					else {
						arrowSprite.graphics.lineTo( xEndNew + (wNew/2), yEndNew  );
					}
					
				}				
								
				
				arrowSprite.graphics.lineStyle(size/2,color,alpha);	
				arrowSprite.graphics.drawEllipse(xEndNew-(wNew/2)-(size/4), yEndNew-(hNew/2)-(size/4), (wNew), (hNew));
					
			}
			else { 				
				arrowSprite.graphics.lineTo( xEndNew, yEndNew );			
				//arrowSprite.graphics.drawEllipse(xEndNew-(wNew/2)-size, yEndNew-(hNew/2)-size, (wNew+size*2), (hNew+size*2));				
				
				arrowSprite.graphics.beginFill(color, alpha);				
				arrowSprite.graphics.drawEllipse(xEndNew-(size), yEndNew-(size), (size*2), (size*2));
				arrowSprite.graphics.endFill();		
			}		
		}
	}
	
	override public function destroy():Void {			
		super.destroy();
		//if (myClickFunc != null) removeEventListener(MouseEvent.CLICK, myClickFunc);
		//myClickFunc = null;	
	}
	
}