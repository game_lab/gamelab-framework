/**
* Main.hx
* version 0.0.03
*  
* Author:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
*  
* Main code for test
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
*/
package;

import Credits;
import MainMenu;
import nme.Lib;

import ar.com.gamelabframework.GUI;
import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.display.Sprite;

import nme.display.StageAlign;
import nme.display.StageScaleMode;

class Main extends Sprite {
	
	// *** Constants ***
	inline static var GAME_CONFIG = "assets/game.xml";	
	// *** Constants ***
	
	// *** Static members ***
	public static var instance:Main;
	
	public static function main():Void {
		Main.instance = new Main();
		
		GUI.assets = Main.instance.gameConfig.get("AssetsId");				
	
		instance.goToScreen(SceneType.Splash);
	}
	// *** Static members ***
	
	// *** Class members ***
	private var currentSceneType:SceneType;
	private var currentScene:LabObject;
	
	public var gdConfig:Map<String, Map<String, String> >;
	public var gameConfig:Map<String, Map<String, String> >;
	public var actionsConfig:Map<String, Map<String, String> >;
	public var blocksConfig:Map<String, Map<String, String> >;
	public var versionConfig:Float;
	
	public var loguedPlayerName:String;
	// Data de coneccion actual
	public var player1Name:String;
	public var player2Name:String;
	public var currentFight:Int;
	
	public var currentPlayerSearch:String;
	
	public var actionIds:List<String>;
	
	
	public function new() {
		super();
		//stage.scaleMode = nme.display.StageScaleMode.EXACT_FIT;
		
		//Lib.current.stage.align = StageAlign.TOP_LEFT;
		//Lib.current.stage.scaleMode = StageScaleMode.NO_SCALE;
		//Lib.current.y = (Lib.stage.stageHeight - 480 * Lib.stage.scaleY) / 2;
		
		gameConfig = loadCfg(GAME_CONFIG);
				
		setConsoleOptions();
		
	}
	
	public function goToScreen(sceneType:SceneType):Void {
		unloadCurrentScene();
		
		currentSceneType = sceneType;
		switch (sceneType) {
			case SceneType.Arrows:
				currentScene = new Arrows();
			case SceneType.Credits:
				currentScene = new Credits();
			case SceneType.Splash:
				currentScene = new Splash();
			case SceneType.MainMenu:
				currentScene = new MainMenu();				
/*				
			case SceneType.Games:
				currentScene = new MainMenu();
			case SceneType.CreateGame:
				currentScene = new CreateGame();
			case SceneType.Fight:
				currentScene = new Fight();
			case SceneType.Scoreboard:
				currentScene = new Scoreboard();
*/
			default:
		}
		if (currentSceneType != SceneType.None)
			loadCurrentScene();
	}
	
	private function loadCurrentScene() {
		nme.Lib.current.addChild(currentScene);
	}
	
	private function unloadCurrentScene() {
		if (currentScene != null) {
			nme.Lib.current.removeChild(currentScene);
			currentScene.destroy();
		}
		
		currentScene = null;
		currentSceneType = SceneType.None;
	}
	
	private function loadCfg(filename:String):Map<String, Map<String, String> > {
		var cfgString:String = nme.Assets.getText(filename);
		var xml:Xml = Xml.parse(cfgString);
		var cfgMap:Map<String, Map<String,String> > = new Map<String, Map<String,String> >();
		
		var rootElement:Xml;
		for (rootElement in xml.elements()) {
			var section:Map<String,String> = new Map<String,String>();
			var variable:Xml;
			for (variable in rootElement.elements())
				section.set(variable.nodeName, variable.firstChild().toString());
			cfgMap.set(rootElement.nodeName, section);
		}
		
		return cfgMap;
	}
	
	private function loadTable(filename:String):Map<String, Map<String, String> > {
		var tableString:String = nme.Assets.getText(filename);
		return parseTable(tableString);
	}
		
	private function parseTable(tableString:String):Map<String, Map<String, String> > {
		var xml:Xml = Xml.parse(tableString);
		//trace("\n"+tableString+"\n");
		var tableMap:Map<String, Map<String,String> > = new Map<String, Map<String,String> >();
		
		xml = xml.elementsNamed("datasheet").next();
		var row:Xml;
		for(row in xml.elementsNamed("row")) {
			var rowData:Map<String,String> = new Map<String,String>();
			
			var cell:Xml;
			for(cell in row.elementsNamed("cell")) {
				rowData.set(cell.get("name"), cell.firstChild().toString());
			}
			
			tableMap.set(row.get("name"), rowData);
		}
		
		return tableMap;
	}
	
	private function loadActionsTable(filename:String):Map<String, Map<String, String> > {
		var tableString:String = nme.Assets.getText(filename);
		return parseActionsTable(tableString);
	}
	
	private function parseActionsTable(tableString:String):Map<String, Map<String, String> > {
		var xml:Xml = Xml.parse(tableString);
		var tableMap:Map<String, Map<String,String> > = new Map<String, Map<String,String> >();
		actionIds = new List<String>();
		
		xml = xml.elementsNamed("datasheet").next();
		var row:Xml;
		for(row in xml.elementsNamed("row")) {
			var rowData:Map<String,String> = new Map<String,String>();
			
			var actionId:String="";
			var lastBlock:Int=0;
			var cell:Xml;
			for(cell in row.elementsNamed("cell")) {
				if (cell.firstChild().toString() == "" || cell.firstChild().toString() == "null") continue;
				if (cell.get("name")=="actionId") {
					actionIds.add(cell.firstChild().toString());
					actionId = cell.firstChild().toString();
					continue;
				}
				if (cell.get("name")=="group") {
					rowData.set(cell.get("name"), cell.firstChild().toString());
					continue;
				}
				var nbr:String = "";
				if (cell.get("name")=="blockId") {
					nbr = Std.string(lastBlock);
					lastBlock++;
				}
				rowData.set(cell.get("name")+nbr, cell.firstChild().toString());
			}
			rowData.set("displayString", row.get("name"));
			
			tableMap.set(actionId, rowData);
		}
		
		return tableMap;
	}
	
	private function setConsoleOptions() {
		var consoleConfig:Map<String,String> = gameConfig.get("Console");
		
		if (consoleConfig.get("Error") == "false")
			Console.showError = false;
		if (consoleConfig.get("Warning") == "false")
			Console.showWarning = false;
		if (consoleConfig.get("Notice") == "false")
			Console.showNotice = false;
		if (consoleConfig.get("Debug") == "false")
			Console.showDebug = false;
	
	}
	// *** Class members **
}
