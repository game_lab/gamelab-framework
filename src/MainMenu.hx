/**
* MainMenu.hx
* version 0.0.03
*  
* Author:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
*  
* Example of menut whith list
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
*/
package;

import ar.com.gamelabframework.GUI;
import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.Assets;
import nme.events.MouseEvent;
import nme.system.System;
import nme.Lib;

import ar.com.gamelabframework.gui.Button;
import ar.com.gamelabframework.gui.Frame;
import ar.com.gamelabframework.gui.Label;		

import nme.events.Event;
import nme.events.IOErrorEvent;

class MainMenu extends GUI {

	private var errorText:Label;
	private var rankText:Label;
	private var listPos:Float;
	private var listRows:Int;
	private var rowHeight:Float;
	private var PageRows:Int;
	private var Page:Int;
	private var eraseDialog:Frame;

	private var tick:Float;
		
	public function new() {
		
		tick = 0;
		
		super();
		frameActive = true;
		buildMenuGUI();
		
		cast(objects.get("disabled"), Button).disableButton();	
			
		//eraseDialog = cast(objects.get("infoDialog"), Frame);			
		//addDialog( (854 - 400) / 2, 10 + (480 - 240) / 2, eraseDialog, "assets/menu/info.xml" );		
		//eraseDialog.visible = false;			
	}
	
	private override function update(deltaTime:Float):Void {
		if ( (listRows - ((Page-1)*PageRows)) > PageRows )
		{
			cast(objects.get("nextPage"), Button).enableButton();					
		}			
		if ( Page > 1 )
		{
			cast(objects.get("prevPage"), Button).enableButton();		
		}
		
		if(  tick != -1 ) {
			tick += deltaTime;
		}			
	}	

	private function buildMenuGUI():Void {
		var menuConfig:Map<String,String> = Main.instance.gameConfig.get("MainMenu");
		
		if (menuConfig == null || !menuConfig.exists("SpecsFile")) {
			Console.error("MainMenu configuration error");
		}
		var menuSpecsFile:String = "assets/" + menuConfig.get("SpecsFile");			
		buildGUI(menuSpecsFile);
		buildMenu();
				
		var playerName:Label = cast(objects.get("playerName"), Label);
		playerName.setText( "Player" );
	}		
		
	private function buildMenu() {
		var listFrame:Frame = cast(objects.get("ListFrame"), Frame);	
		var listTitleFrame:Frame = cast(objects.get("ListTitlesFrame"), Frame);	
		listPos = listFrame.getDisplacementY();
		//listFrame.addGUIObject(button, "fight");		
		
		listRows = 23;
		var h = listFrame.height-5;		
		addFilaButton( "title", "ListTitlesFrame", "Black75", 10, 5, (listTitleFrame.width - 20), 30, "Name", "", "", 1, 12, false);

		var i = 0;
		var buttonY:Float = 5;
		rowHeight = 55;
		buttonY = addFilaButton( "button"+i, "ListFrame", "Green", 10, buttonY, (listTitleFrame.width - 20), 50, "Arrows Test", "Arrows", "", 4, 14,false) +5;
		i++;
		i++;
		while( i<listRows)
		{
			buttonY = addFilaButton( "button"+i, "ListFrame", "Green", 10, buttonY, (listTitleFrame.width - 20), 50, "button n* "+i, "", "", 4, 14,false) +5;
			i++;
		}
		
		Page = 1;	
		PageRows = cast(Math.floor(h/rowHeight),Int);
		
		cast(objects.get("nextPage"), Button).disableButton();		
		cast(objects.get("prevPage"), Button).disableButton();		
		
		if (listRows > PageRows )
		{
			cast(objects.get("nextPage"), Button).enableButton();					
		}		
		cast(objects.get("pageInfo"), Label).setText("Page: " + Page + "/" + (1 + Math.floor(listRows / PageRows)));	
	}
		
	private function addFilaButton(buttonId:String, frameId:String, assetId:String, buttonX:Float , buttonY:Float, buttonW:Float, buttonH:Float, tag:String, type:String, data:String, border=0, fontsize=15, activo:Bool=true) {
		var listFrame:Frame = cast(objects.get(frameId), Frame);
		
		var buttonString:String = "<Button id=\""+buttonId+"\" text=\"" + tag +"\"";
		buttonString += " x=\""+buttonX+"\" y=\""+buttonY+"\" w=\""+buttonW+"\" h=\""+buttonH+"\"";
		buttonString += " assetId=\""+assetId+"\" greyAssetId=\"Grey\"";
		buttonString += " border=\""+border+"\"";
		buttonString += " data=\"" + data + "\"";	
		buttonString += " clickFnc=\"" + type + "\" font=\"bdcartoonshout\" fontSize=\""+fontsize+"\" />";
		var buttonXml:Xml = Xml.parse(buttonString).elements().next();
		
		var button:Button = GUI.createButton(buttonXml, this);
		
		listFrame.addGUIObject(button, "b"+buttonX+"-"+buttonY);	
		if (!activo)
		{
			//button.disableButton();
		}
		return buttonY + buttonH;
	}	
	
	private override function getFunction(funcName:String):Dynamic->Void {
		switch (funcName) {			
			case "Arrows":
				return goArrows;
			case "Next":
				return nextPage;
			case "Prev":
				return prevPage;
			case "about":
				return credits;
			case "Slider":
				return slider;
			default:
				Console.warning("Function "+funcName+" not valid, skipping");
				return function(notUsed:Dynamic) {};
		}
	}
		
	private function credits(event:MouseEvent):Void {		
		Console.debug("credits clicked");
		event.currentTarget.disableButton();			
		Main.instance.goToScreen(SceneType.Credits);					
	}

	private function goArrows(event:MouseEvent):Void {		
		Console.debug("arrows clicked");
		event.currentTarget.disableButton();			
		Main.instance.goToScreen(SceneType.Arrows);					
	}
	
	private function slider(event:MouseEvent):Void {
		Console.debug("Slider to " + event.currentTarget.handleValue);
	}
		
	private function nextPage(event:MouseEvent):Void {
		Console.debug("nextPage clicked");

		event.currentTarget.disableButton();			
		var listFrame:Frame = cast(objects.get("ListFrame"), Frame);			
		listFrame.setDisplacementY( listFrame.getDisplacementY() + (PageRows * rowHeight));
		Page++;	
		cast(objects.get("nextPage"), Button).disableButton();		
		cast(objects.get("prevPage"), Button).disableButton();		
		cast(objects.get("pageInfo"), Label).setText("Page: "+Page+"/"+(1+Math.floor(listRows/PageRows)));
	}

	private function prevPage(event:MouseEvent):Void {
		Console.debug("nextPage clicked");

		event.currentTarget.disableButton();			
		var listFrame:Frame = cast(objects.get("ListFrame"), Frame);	
		listFrame.setDisplacementY(listFrame.getDisplacementY() - (PageRows * rowHeight));
		Page--;	
		cast(objects.get("nextPage"), Button).disableButton();		
		cast(objects.get("prevPage"), Button).disableButton();		
		cast(objects.get("pageInfo"), Label).setText("Page: "+Page+"/"+(1+Math.floor(listRows/PageRows)));
	}
}