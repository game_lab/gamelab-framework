/**
* Credits.hx
* version 0.0.03
*  
* Author:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* 
* Creedits screen example
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
*/
package;

import ar.com.gamelabframework.GUI;
import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.Assets;
import nme.events.MouseEvent;
import nme.system.System;
import nme.Lib;
import ar.com.gamelabframework.gui.Frame;
import ar.com.gamelabframework.gui.Label;
import ar.com.gamelabframework.gui.Button;

class Credits extends GUI {
	
	private var tick:Float;
	
	public function new() {
		super();
		tick=0;
		frameActive = true;
		buildMenuGUI();
	}
	
	private function buildMenuGUI():Void {
		var menuConfig:Map<String,String> = Main.instance.gameConfig.get("Credits");
		if (menuConfig == null || !menuConfig.exists("SpecsFile")) {
			Console.error("Credits configuration error");
			#if (cpp)
			Sys.exit(1);
			#end
		}
		var menuSpecsFile:String = "assets/"+menuConfig.get("SpecsFile");
		buildGUI(menuSpecsFile);
				
	}

	private override function update(deltaTime:Float):Void {
		tick += deltaTime;
		//Console.tarce(tick);
		if (tick > 20)
			play(null);
	}
	
	private override function getFunction(funcName:String):Dynamic->Void {
		switch (funcName) {
			case "Play":
				return play;
			case "Version":
				return function(notUsed:Dynamic) {};
			default:
				Console.warning("Function "+funcName+" not valid, skipping");
				return function(notUsed:Dynamic) {};
		}
	}
	
	private function play(event:MouseEvent):Void {
		Console.debug("Play clicked");
		frameActive = false;
		Main.instance.goToScreen(SceneType.MainMenu);
		//Main.instance.goToScreen(SceneType.Login);
	}
	
}