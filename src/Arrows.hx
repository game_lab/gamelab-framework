/**
* Arrows.hx
* version 0.0.03
*  
* Author:
* Eduardo Rodriguez Ortega <eromail@gmail.com> 
* 
* Creedits screen example
* 
* Copyright (c) 2012 Eduardo Rodriguez Ortega
*/
package;

import ar.com.gamelabframework.GUI;
import ar.com.gamelabframework.LabObject;
import ar.com.gamelabframework.utils.Console;
import nme.Assets;
import nme.events.MouseEvent;
import nme.system.System;
import nme.Lib;
import ar.com.gamelabframework.gui.Frame;
import ar.com.gamelabframework.gui.Label;
import ar.com.gamelabframework.gui.Button;

class Arrows extends GUI {
	
	private var tick:Float;
	
	public function new() {
		super();
		tick=0;
		frameActive = true;
		buildMenuGUI();
	}
	
	private function buildMenuGUI():Void {
		var menuConfig:Map<String,String> = Main.instance.gameConfig.get("Arrows");
		if (menuConfig == null || !menuConfig.exists("SpecsFile")) {
			Console.error("Credits configuration error");
			#if (cpp)
			Sys.exit(1);
			#end
		}
		var menuSpecsFile:String = "assets/"+menuConfig.get("SpecsFile");
		buildGUI(menuSpecsFile);
				
	}

	private override function update(deltaTime:Float):Void {
		tick += deltaTime;
		//Console.tarce(tick);
		//if (tick > 20)
			//play(null);
	}
	
	private override function getFunction(funcName:String):Dynamic->Void {
		switch (funcName) {
			case "back":
				return back;
			case "Version":
				return function(notUsed:Dynamic) {};
			default:
				Console.warning("Function "+funcName+" not valid, skipping");
				return function(notUsed:Dynamic) {};
		}
	}
	
	private function back(event:MouseEvent):Void {
		Console.debug("Back clicked");
		frameActive = false;
		Main.instance.goToScreen(SceneType.MainMenu);
	}
	
}